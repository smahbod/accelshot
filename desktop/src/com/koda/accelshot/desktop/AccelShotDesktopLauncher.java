package com.koda.accelshot.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.koda.accelshot.AccelShot;

public class AccelShotDesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = (int) Math.ceil(AccelShot.WIDTH);
		config.height = (int) Math.ceil(AccelShot.HEIGHT);
		config.title = AccelShot.TITLE;
		config.y = 0;
		new LwjglApplication(new AccelShot(), config);
	}
}
