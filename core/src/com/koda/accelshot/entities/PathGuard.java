package com.koda.accelshot.entities;

import com.badlogic.gdx.math.MathUtils;
import com.koda.accelshot.handlers.Constants;
import com.koda.accelshot.handlers.Path;
import com.koda.accelshot.handlers.RotationWaypoint;

public class PathGuard extends Guard {

	protected float targetAngle;
	protected float distanceTraveled;
	protected float distanceNeeded;
	protected float delayTimer;
	protected float rotationDelayTimer;
	protected float initialRadians;
	protected int rotationpathnum = 0;
	protected int pathnum = 0;
	protected Path[] path;
	
	private float rotateOption1;
	private float rotateOption2;
	private boolean changingPaths;
	private boolean executingRotationSequence;
	private boolean pathSwitchCompleted = true;
	private int rotationIterations = 0;
	
	public PathGuard(float x, float y, float width, float height, float radians, Path[] path, int pathnum) {
		super(x, y, width, height);
		this.pathnum = pathnum;
		this.radians = radians;
		this.initialRadians = radians;
		if (path == null) {
			System.out.println("there was a null path");
			this.path = new Path[] {new Path(x, y, 0, 0, null)};
		} else {
			this.path = path;
		}
		updateAnimationAngle();
	}
	
	//bare minimum constructor
	public PathGuard(float width, float height, Path[] path, int pathnum) {
		this(path[pathnum].x, path[pathnum].y, width, height, 0, path, pathnum);
	}
	
	protected int rotate(float dt) {
		RotationWaypoint[] rw = path[pathnum].rw;
		float amount = rw[rotationpathnum].direction * dt * rw[rotationpathnum].turnRate;
		radians += amount;
		updateAnimationAngle();
		
		//rotate indefinitely if length is 1 and distance needed is 0
		if (!(rw.length == 1 && rw[0].distance == 0)) {
			rotationAccum += Math.abs(amount);
			if (rotationAccum >= Math.abs(rw[rotationpathnum].distance)) {
				rotationAccum = 0;
				//the radians will always overshoot, so correct it
				radians = initialRadians + rw[rotationpathnum].distance * rw[rotationpathnum].direction;
				initialRadians = radians;
				//System.out.println("Finished rotating of " + rotationpathnum);
				rotationpathnum = (rotationpathnum + 1) % rw.length;
				rotationDelayTimer = rw[rotationpathnum].delay;
				rotationIterations++;
			}
		}
		
		return rotationIterations;
	}
	
	protected void updatePath(float dt) {
		if (pathSwitchCompleted && distanceTraveled >= distanceNeeded) {
			//reached checkpoint
			animation.resetFrame();
			pathSwitchCompleted = false;
			distanceTraveled = 0;
			x = path[pathnum].x;
			y = path[pathnum].y;
			//changingPaths = true;
			if (path[pathnum].rw != null) {
				animation.stop();
				executingRotationSequence = true;
				rotationpathnum = 0;
				rotationDelayTimer = path[pathnum].rw[rotationpathnum].delay;
				initialRadians = radians;
				//System.out.println("Reached end of " + pathnum);
			} else {
				changingPaths = true;
				pathnum = (pathnum + 1) % path.length;
				delayTimer = path[pathnum].delay;
			}
		}
		
		if (executingRotationSequence) {
			if (rotationDelayTimer > 0)
				rotationDelayTimer -= dt;
			if (rotationDelayTimer <= 0) {
				if (rotate(dt) == path[pathnum].rw.length) {
					rotationIterations = 0;
					executingRotationSequence = false;
					changingPaths = true;
					pathnum = (pathnum + 1) % path.length;
					delayTimer = path[pathnum].delay;
					//System.out.println("Finished rotation sequence");
				}
			}
		}
		
		if (changingPaths) {
			
			if (delayTimer == 0) {
				//delayTimer = 0;
				rotating = true;
				changingPaths = false;
				speed = path[pathnum].speed;
				
				distanceNeeded = distanceTo(path[pathnum].x, path[pathnum].y);
				//distanceTraveled = 0;
				if (path.length > 1)
					targetAngle = MathUtils.atan2(path[pathnum].y - y, path[pathnum].x - x);
				else
					targetAngle = radians;
				while (targetAngle < 0) {
					targetAngle += 2*pi;
				}
				
				rotateOption1 = Math.abs(radians - targetAngle);
				rotateOption2 = 2*pi - rotateOption1;
				
				if (targetAngle > radians)
					rotationDirection = rotateOption1 < rotateOption2 ? COUNTER_CLOCKWISE : CLOCKWISE;
				else
					rotationDirection = rotateOption1 < rotateOption2 ? CLOCKWISE : COUNTER_CLOCKWISE;
			}
		}
			
		if (rotating) {
			radians += rotationDirection * dt * turnRate;
			rotationAccum += dt * turnRate;
			updateAnimationAngle();
			
			float radiansDistanceNeeded = Math.min(rotateOption1, rotateOption2);
			
			if (rotationAccum >= radiansDistanceNeeded) {
				animation.start();
				rotating = false;
				pathSwitchCompleted = true;
				radians = targetAngle;
				wobbled = 1;
				wobbleAccum = 0;
				rotationAccum = 0;
				
				if (rotationDirection == COUNTER_CLOCKWISE) {
					wobbleMode = true;
				} else {
					wobbleMode = false;
				}
			}
		}
				
		//move
		if (!executingRotationSequence && !changingPaths && !rotating) {
			dx = speed * dt * MathUtils.cos(targetAngle);
			dy = speed * dt * MathUtils.sin(targetAngle);
			x += dx;
			y += dy;
			distanceTraveled += Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
		}
	}
	
	@Override
	public void update(float dt) {
		super.update(dt);
		if (delayTimer > 0) {
			delayTimer -= dt;
			if (delayTimer < 0)
				delayTimer = 0;
		}
		if (rotationDelayTimer > 0) {
			rotationDelayTimer -= dt;
			if (rotationDelayTimer < 0)
				rotationDelayTimer = 0;
		}
		if (path.length > 0) {
			updatePath(dt);
		}
	}
	
	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < path.length; i++) {
			s += path[i];
			if (i != path.length - 1)
				s += "\n";
		}
		return "Path" + super.toString() + ", path = " + s;
	}
}
