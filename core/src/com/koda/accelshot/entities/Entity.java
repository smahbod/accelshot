package com.koda.accelshot.entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.koda.accelshot.AccelShot;

public abstract class Entity {

	public static final float pi = MathUtils.PI;
	public static final float STD_WIDTH = AccelShot.TILE_SIZE * AccelShot.SCALE;
	public static final float STD_HEIGHT = AccelShot.TILE_SIZE * AccelShot.SCALE;
	
	public float x;
	public float y;
	public float width;
	public float height;
	
	protected float dx;
	protected float dy;
	
	protected float radians;
	protected float speed;
	protected float rotationSpeed;
	
	protected float[] shapex;
	protected float[] shapey;
	protected float[] shapePolygon;
	
	protected boolean shouldRemove;
	
	public float getX() { return x; }
	public float getY() { return y; }
	public float getDX() { return dx; }
	public float getDY() { return dy; }
	public float getSpeed() { return speed; }
	public boolean shouldRemove() { return shouldRemove; }
	public float[] getBodyX() { return shapex; }
	public float[] getBodyY() { return shapey; }
	
	public void setRemove(boolean b) {
		shouldRemove = b;
	}
	
	public void setMotion(float dx, float dy) {
		this.dx = dx;
		this.dy = dy;
	}
	
	protected void wrap() {
		if (x < 0)
			x = AccelShot.WIDTH;
		if (x > AccelShot.WIDTH)
			x = 0;
		if (y < 0)
			y = AccelShot.HEIGHT;
		if (y > AccelShot.HEIGHT)
			y = 0;
	}
	
	public float distanceTo(float x, float y) {
		return (float) Math.sqrt(Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2));
	}
	
	public boolean isOutOfBounds() {
		return x - width / 2 < 0 || x + width / 2 > AccelShot.WIDTH || y - height / 2 < 0 || y + height / 2 > AccelShot.HEIGHT;
	}
	
	public boolean isCollidingWith(Entity e) {
		for (int i = 0; i < e.shapex.length; i++) {
			if (isCollidingWith(e.shapex[i], e.shapey[i])) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isCollidingWith(float[] sx, float[] sy) {
		boolean b = false;
		for (int i = 0; i < sx.length; i++) {
			int j = (i + 1) % sx.length;
			if (((sy[i] > y) != (sy[j] > y)) && (x < (sx[j] - sx[i]) * (y - sy[i]) / (sy[j] - sy[i]) + sx[i])) {
				b = !b;
			}
		}
		return b;
	}
	
	public boolean isCollidingWith(float x, float y) {
		boolean b = false;
		for (int i = 0; i < shapex.length; i++) {
			int j = (i + 1) % shapex.length;
			if (((shapey[i] > y) != (shapey[j] > y)) && (x < (shapex[j] - shapex[i]) * (y - shapey[i]) / (shapey[j] - shapey[i]) + shapex[i])) {
				b = !b;
			}
		}
		return b;
	}
	
	public abstract void updateBody();
	public abstract void update(float dt);
	public abstract void render(SpriteBatch sb, ShapeRenderer sr);
}
