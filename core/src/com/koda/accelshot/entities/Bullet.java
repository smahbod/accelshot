package com.koda.accelshot.entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.koda.accelshot.AccelShot;
import com.koda.accelshot.states.State;

public class Bullet extends Entity implements Comparable<Bullet> {

	private final float fastSpeed = 300;
	private final float slowSpeed = 50;
	private final float maxDistance = -1;
	private final int curveSegments = 5;
	private float initialX;
	private float initialY;
	private int id;
	
	public Bullet(float x, float y, float radians, int id) {
		speed = slowSpeed;
		this.x = initialX = x;
		this.y = initialY = y;
		this.radians = radians;
		this.width = this.height = 4;
		this.id = id;
		shapex = new float[curveSegments];
		shapey = new float[curveSegments];
		shapePolygon = new float[shapex.length * 2];
		updateBody();
	}
	
	public int getID() {
		return id;
	}
	
	public void toggleSpeed() {
		if (speed == slowSpeed) {
			speed = fastSpeed;
		} else {
			speed = slowSpeed;
		}
	}
	
	public void updateBody() {
		float theta = radians;
		for (int i = 0; i < shapex.length; i++) {
			shapex[i] = x + MathUtils.cos(theta) * width / 2;
			shapey[i] = y + MathUtils.sin(theta) * height / 2;
			shapePolygon[i * 2] = shapex[i];
			shapePolygon[i * 2 + 1] = shapey[i];
			theta += 2*pi / curveSegments;
		}
	}
	
	@Override
	public int compareTo(Bullet b) {
		return this.id - b.id;
	}

	@Override
	public void update(float dt) {
		x += dt * speed * MathUtils.cos(radians);
		y += dt * speed * MathUtils.sin(radians);
		
		//distance formula
		if (maxDistance != -1) {
			float dist = (float) Math.sqrt(Math.pow(x - initialX, 2) + Math.pow(y - initialY, 2));
			if (dist > maxDistance) {
				shouldRemove = true;
			}
		}
		
		if (isOutOfBounds()) {
			shouldRemove = true;
		}
		
		updateBody();
	}

	@Override
	public void render(SpriteBatch sb, ShapeRenderer sr) {
		sb.setProjectionMatrix(State.camera.combined);
		sb.begin();
		AccelShot.etext.getFont().setColor(0, 1, 0, 1);
		AccelShot.etext.getFont().draw(sb, Integer.toString(id), x + 5, y + 5);
		sb.end();

		
		sr.setColor(1, 0, 0, 1);
		sr.begin(ShapeType.Line);
		sr.identity();
		sr.polygon(shapePolygon);
		sr.end();
	}
}
