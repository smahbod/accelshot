package com.koda.accelshot.entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.koda.accelshot.AccelShot;
import com.koda.accelshot.states.State;

public class CollisionBox extends Entity {

	public int index;
	
	public CollisionBox(float x, float y, float width, float height, int index) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.index = index;
		shapex = new float[4];
		shapey = new float[4];
		shapePolygon = new float[shapex.length * 2];
		updateBody();
	}
	
	@Override
	public void updateBody() {
		shapex[0] = x - width / 2;
		shapey[0] = y + height / 2;
		
		shapex[1] = x - width / 2;
		shapey[1] = y - height / 2;
		
		shapex[2] = x + width / 2;
		shapey[2] = y - height / 2;
		
		shapex[3] = x + width / 2;
		shapey[3] = y + height / 2;
		
		for (int i = 0; i < shapex.length; i++) {
			shapePolygon[i * 2] = shapex[i];
			shapePolygon[i * 2 + 1] = shapey[i];
		}
	}
	
	public boolean contains(float x, float y) {
		return x >= this.x - width / 2 && x < this.x + width / 2
				&& y >= this.y - height / 2 && y < this.y + this.height / 2;
	}
	
	public boolean entirelyContains(Entity e) {
		return e.x - e.width / 2 >= this.x - this.width / 2
				&& e.x + e.width / 2 < this.x + this.width / 2
				&& e.y - e.height / 2 >= this.y - this.height / 2
				&& e.y + e.height / 2 < this.y + e.height / 2;
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(SpriteBatch sb, ShapeRenderer sr) {
		if (AccelShot.DEBUG) {
			sr.identity();
			sr.setProjectionMatrix(State.camera.combined);
			sr.setColor(108f/255f, 15f/255f, 148f/255f, 1);
			sr.begin(ShapeType.Line);
			sr.polygon(shapePolygon);
			sr.end();
			
			//sb.setProjectionMatrix(State.camera.combined);
			sb.begin();
			float posx = x;// - AccelShot.etext.getFpsFont().getBounds(Integer.toString(index)).width / 2;
			float posy = y;// + AccelShot.etext.getFpsFont().getBounds(Integer.toString(index)).height / 1;
			AccelShot.etext.getFpsFont().draw(sb, Integer.toString(index), posx, posy);
			sb.end();
		}
	}
	
	@Override
	public String toString() {
		return "Box " + index + ": x = " + x + ", y = " + y + ", width = " + width + ", height = " + height;
	}
}
