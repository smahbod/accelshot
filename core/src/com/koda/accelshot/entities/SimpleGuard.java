package com.koda.accelshot.entities;

public class SimpleGuard extends Guard {

	public SimpleGuard(float x, float y, float width, float height, float startRadians) {
		super(x, y, width, height);
		this.radians = startRadians;
		updateAnimationAngle();
	}
	
	@Override
	public String toString() {
		return "Simple" + super.toString();
	}
}
