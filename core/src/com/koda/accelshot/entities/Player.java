package com.koda.accelshot.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.koda.accelshot.AccelShot;
import com.koda.accelshot.handlers.Constants;
import com.koda.accelshot.handlers.LevelReader;
import com.koda.accelshot.states.State;
import com.koda.accelshot.ui.Animation;

public class Player extends Entity {

	private final int bodyCurveSegments = 12;
	private float tipx;
	private float tipy;
	private Array<Bullet> bullets;
	private Animation animation;
	private boolean[] directions;
	private int bulletsUsed;
	
	public Player(float x, float y, float width, float height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		//shapex = new float[4];
		//shapey = new float[4];
		shapex = new float[bodyCurveSegments];
		shapey = new float[bodyCurveSegments];
		shapePolygon = new float[shapex.length * 2];
		directions = new boolean[4];
		
		radians = 0;
		speed = 100;
		
		Texture tex = new Texture(Gdx.files.internal("accelshot_character_spritesheet_16_final_clear.png"));
		TextureRegion[][] frames = TextureRegion.split(tex, AccelShot.TILE_SIZE, AccelShot.TILE_SIZE);
		animation = new Animation(frames, .1875f);
		
		updateBody();
	}
	
	public void setBullets(Array<Bullet> bullets) {
		this.bullets = bullets;
	}
	
	public void setDirection(int dir, boolean set) {
		directions[dir] = set;
		
		if (directions[Constants.DIRECTION_RIGHT] && directions[Constants.DIRECTION_UP]) {
			directions[Constants.DIRECTION_RIGHT] = false;
		}
		
		if (directions[Constants.DIRECTION_LEFT] && directions[Constants.DIRECTION_UP]) {
			directions[Constants.DIRECTION_LEFT] = false;
		}
		
		if (directions[Constants.DIRECTION_RIGHT] && directions[Constants.DIRECTION_DOWN]) {
			directions[Constants.DIRECTION_RIGHT] = false;
		}
		
		if (directions[Constants.DIRECTION_LEFT] && directions[Constants.DIRECTION_DOWN]) {
			directions[Constants.DIRECTION_LEFT] = false;
		}
	}
	
	public void updateBody() {
		/*shapex[0] = x + MathUtils.cos(0 + radians) * 15;
		shapey[0] = y + MathUtils.sin(0 + radians) * 15;
		
		shapex[1] = x + MathUtils.cos(pi / 2 + radians) * 15;
		shapey[1] = y + MathUtils.sin(pi / 2 + radians) * 15;
		
		shapex[2] = x + MathUtils.cos(pi + radians) * 15;
		shapey[2] = y + MathUtils.sin(pi + radians) * 15;
		
		shapex[3] = x + MathUtils.cos(-pi / 2 + radians) * 15;
		shapey[3] = y + MathUtils.sin(-pi / 2 + radians) * 15;*/
		
		float theta = radians;
		for (int i = 0; i < shapex.length; i++) {
			shapex[i] = x + MathUtils.cos(theta) * width / 2;
			shapey[i] = y + MathUtils.sin(theta) * height / 2;
			shapePolygon[i * 2] = shapex[i];
			shapePolygon[i * 2 + 1] = shapey[i];
			theta += 2*pi / bodyCurveSegments;
		}
		
		tipx = x + MathUtils.cos(radians) * width;
		tipy = y + MathUtils.sin(radians) * height;
	}
	
	private void normalizeRadians() {
		while (radians < 0) {
			radians += 2*pi;
		}
		
		while (radians >= 2*pi) {
			radians -= 2*pi;
		}
	}
	
	public void setRadians(float rad) {
		radians = rad;
	}
	
	public void addBullet() {
		if (bullets.size == 5)
			return;
		
		int index = 1;
		for (int i = 0; i < bullets.size; i++) {
			if (bullets.get(i).getID() == index) {
				index++;
			}
		}
		bullets.add(new Bullet(tipx, tipy, radians, index < 10 ? index : 0));
		bullets.sort();
		bulletsUsed++;
	}
	
	public void toggleBulletSpeed(int id) {
		for (int i = 0; i < bullets.size; i++) {
			if (bullets.get(i).getID() == id) {
				bullets.get(i).toggleSpeed();
			}
		}
	}
	
	public void setDead() {
		//TODO: do other stuff in here to handle dead case
		setRemove(true);
	}
	
	public int getBulletsUsed() {
		return bulletsUsed;
	}
	
	private void movePlayer(float dt) {
		float oldX = x;
		float oldY = y;
		
		if (directions[Constants.DIRECTION_RIGHT]) {
			x += dt * speed;
		}
		
		if (directions[Constants.DIRECTION_UP]) {
			y += dt * speed;
		}
		
		if (directions[Constants.DIRECTION_LEFT]) {
			x -= dt * speed;
		}
		
		if (directions[Constants.DIRECTION_DOWN]) {
			y -= dt * speed;
		}
		
		if (isOutOfBounds()) {
			revertPosition(oldX, oldY);
		}
		
		for (int i = 0; i < LevelReader.getCollisionBoxes().size; i++) {
			CollisionBox cb = LevelReader.getCollisionBoxes().get(i);
			if (directions[Constants.DIRECTION_LEFT]) {
				if (cb.contains(x - width / 2, y) || this.isCollidingWith(cb)) {
					revertPosition(oldX, oldY);
					//can climb it
					boolean canClimbUp = true;
					boolean canClimbDown = true;
					for (CollisionBox other : LevelReader.getCollisionBoxes()) {
						if (other != cb) {
							if (y < cb.y + cb.height / 2 || other.contains(cb.x, cb.y + cb.height)) {
								canClimbUp = false;
							}
							
							if (y > cb.y - cb.height / 2 || other.contains(cb.x, cb.y - cb.height)) {
								canClimbDown = false;
							}
						}
						
					}
					
					if (canClimbUp) {
						y += dt * speed;
					}
					if (canClimbDown) {
						y -= dt * speed;
					}	
				}
			}
			
			if (directions[Constants.DIRECTION_DOWN]) {
				if (cb.contains(x, y - height / 2) || this.isCollidingWith(cb)) {
					revertPosition(oldX, oldY);
					boolean canClimbRight = true;
					boolean canClimbLeft = true;
					for (CollisionBox other : LevelReader.getCollisionBoxes()) {
						if (other != cb) {
							if (x < cb.x + cb.width / 3 || other.contains(cb.x + cb.width, cb.y)) {
								canClimbRight = false;
							}
							
							if (x > cb.x - cb.width / 3 || other.contains(cb.x - cb.width, cb.y)) {
								canClimbLeft = false;
							}
						}
						
					}
					
					if (canClimbRight) {
						x += dt * speed;
					}
					if (canClimbLeft) {
						x -= dt * speed;
					}
				}
			}
			
			if (directions[Constants.DIRECTION_RIGHT]) {
				if (cb.contains(x + width / 2, y) || this.isCollidingWith(cb)) {
					revertPosition(oldX, oldY);
					boolean canClimbUp = true;
					boolean canClimbDown = true;
					for (CollisionBox other : LevelReader.getCollisionBoxes()) {
						if (other != cb) {
							if (y < cb.y + cb.height / 2 || other.contains(cb.x, cb.y + cb.height)) {
								canClimbUp = false;
							}
							
							if (y > cb.y - cb.height / 2 || other.contains(cb.x, cb.y - cb.height)) {
								canClimbDown = false;
							}
						}
						
					}
					
					if (canClimbUp) {
						y += dt * speed;
					}
					if (canClimbDown) {
						y -= dt * speed;
					}	
				}
			}
			
			if (directions[Constants.DIRECTION_UP]) {
				if (cb.contains(x, y + height / 2) || this.isCollidingWith(cb)) {
					revertPosition(oldX, oldY);
					boolean canClimbRight = true;
					boolean canClimbLeft = true;
					for (CollisionBox other : LevelReader.getCollisionBoxes()) {
						if (other != cb) {
							if (x < cb.x + cb.width / 3 || other.contains(cb.x + cb.width, cb.y)) {
								canClimbRight = false;
							}
							
							if (x > cb.x - cb.width / 3 || other.contains(cb.x - cb.width, cb.y)) {
								canClimbLeft = false;
							}
						}
						
					}
					
					if (canClimbRight) {
						x += dt * speed;
					}
					if (canClimbLeft) {
						x -= dt * speed;
					}
				}
			}
		}
	}
	
	private void revertPosition(float oldX, float oldY) {
		x = oldX;
		y = oldY;
		updateBody();
	}
	
	private void determineAnimationRow() {
		if (!anyDirectionsPressed()) {
			if (radians >= 15*pi/8 || radians < pi/8) {
				animation.setAnimationRow(Constants.ANIMATION_RIGHT);
			} else if (radians >= pi/8 && radians < 3*pi/8) {
				animation.setAnimationRow(Constants.ANIMATION_RIGHT_UP);
			} else if (radians >= 3*pi/8 && radians < 5*pi/8) {
				animation.setAnimationRow(Constants.ANIMATION_UP);
			} else if (radians >= 5*pi/8 && radians < 7*pi/8) {
				animation.setAnimationRow(Constants.ANIMATION_LEFT_UP);
			} else if (radians >= 7*pi/8 && radians < 9*pi/8) {
				animation.setAnimationRow(Constants.ANIMATION_LEFT);
			} else if (radians >= 9*pi/8 && radians < 11*pi/8) {
				animation.setAnimationRow(Constants.ANIMATION_LEFT_DOWN);
			} else if (radians >= 11*pi/8 && radians < 13*pi/8) {
				animation.setAnimationRow(Constants.ANIMATION_DOWN);
			} else {
				animation.setAnimationRow(Constants.ANIMATION_RIGHT_DOWN);
			}
		}
		
		if (directions[Constants.DIRECTION_DOWN]) {
			if (radians >= 5*pi/4 && radians <= 7*pi/4) {
				animation.setAnimationRow(Constants.ANIMATION_DOWN);
			} else if (radians >= pi && radians < 5*pi/4) {
				animation.setAnimationRow(Constants.ANIMATION_DOWN_LEFT);
			} else if (radians > 7*pi/4 && radians <= 2*pi) {
				animation.setAnimationRow(Constants.ANIMATION_DOWN_RIGHT);
			} else if (radians >= pi/4 && radians <= 3*pi/4) {
				animation.setAnimationRow(Constants.ANIMATION_UP);
			} else if (radians > 3*pi/4 && radians <= pi) {
				animation.setAnimationRow(Constants.ANIMATION_UP_LEFT);
			} else if (radians >= 0 && radians < pi/4) {
				animation.setAnimationRow(Constants.ANIMATION_UP_RIGHT);
			}
		}
		
		if (directions[Constants.DIRECTION_UP]) {
			if (radians >= pi/4 && radians <= 3*pi/4) {
				animation.setAnimationRow(Constants.ANIMATION_UP);
			} else if (radians > 3*pi/4 && radians <= pi) {
				animation.setAnimationRow(Constants.ANIMATION_UP_LEFT);
			} else if (radians >= 0 && radians < pi/4) {
				animation.setAnimationRow(Constants.ANIMATION_UP_RIGHT);
			} else if (radians >= 5*pi/4 && radians <= 7*pi/4) {
				animation.setAnimationRow(Constants.ANIMATION_DOWN);
			} else if (radians >= pi && radians < 5*pi/4) {
				animation.setAnimationRow(Constants.ANIMATION_DOWN_LEFT);
			} else if (radians > 7*pi/4 && radians <= 2*pi) {
				animation.setAnimationRow(Constants.ANIMATION_DOWN_RIGHT);
			}
		}
		
		if (directions[Constants.DIRECTION_RIGHT]) {
			if (radians >= 7*pi/4 || radians <= pi/4) {
				animation.setAnimationRow(Constants.ANIMATION_RIGHT);
			} else if (radians >= 3*pi/2 && radians < 7*pi/4) {
				animation.setAnimationRow(Constants.ANIMATION_RIGHT_DOWN);
			} else if (radians > pi/4 && radians <= pi/2) {
				animation.setAnimationRow(Constants.ANIMATION_RIGHT_UP);
			} else if (radians >= 3*pi/4 && radians <= 5*pi/4) {
				animation.setAnimationRow(Constants.ANIMATION_LEFT);
			} else if (radians > 5*pi/4 && radians <= 3*pi/2) {
				animation.setAnimationRow(Constants.ANIMATION_LEFT_DOWN);
			} else if (radians >= pi/2 && radians < 3*pi/4) {
				animation.setAnimationRow(Constants.ANIMATION_LEFT_UP);
			}
		}
		
		if (directions[Constants.DIRECTION_LEFT]) {
			if (radians >= 3*pi/4 && radians <= 5*pi/4) {
				animation.setAnimationRow(Constants.ANIMATION_LEFT);
			} else if (radians > 5*pi/4 && radians <= 3*pi/2) {
				animation.setAnimationRow(Constants.ANIMATION_LEFT_DOWN);
			} else if (radians >= pi/2 && radians < 3*pi/4) {
				animation.setAnimationRow(Constants.ANIMATION_LEFT_UP);
			} else if (radians >= 7*pi/4 || radians <= pi/4) {
				animation.setAnimationRow(Constants.ANIMATION_RIGHT);
			} else if (radians >= 3*pi/2 && radians < 7*pi/4) {
				animation.setAnimationRow(Constants.ANIMATION_RIGHT_DOWN);
			} else if (radians > pi/4 && radians <= pi/2) {
				animation.setAnimationRow(Constants.ANIMATION_RIGHT_UP);
			}
		}
	}
	
	private boolean anyDirectionsPressed() {
		for (int i = 0; i < directions.length; i++) {
			if (directions[i]) {
				return true;
			}
		}
		return false;
	}
	
	private void determineAnimationPlaying() {
		if (anyDirectionsPressed()) {
			animation.start();
		} else {
			animation.resetFrame();
			animation.stop();
		}
	}
	
	@Override
	public void update(float dt) {
		movePlayer(dt);
		//wrap();
		normalizeRadians();
		updateBody();
		animation.update(dt);
		determineAnimationRow();
		determineAnimationPlaying();
	}
	
	@Override
	public void render(SpriteBatch sb, ShapeRenderer sr) {
		sb.setProjectionMatrix(State.camera.combined);
		sb.begin();
		sb.setColor(1, 1, 1, 1);
		TextureRegion image = animation.getFrame();
		float x =  this.x - image.getRegionWidth() / 2;
		float y =  this.y - image.getRegionHeight() / 2;
		sb.draw(image, x, y, image.getRegionWidth() / 2, image.getRegionHeight() / 2, 
				image.getRegionWidth(), image.getRegionHeight(), AccelShot.SCALE, AccelShot.SCALE, 0f);
		sb.end();
		
		if (AccelShot.DEBUG) {
			sr.identity();
			sr.setColor(1, 1, 0, 1);
			sr.begin(ShapeType.Line);
			sr.polygon(shapePolygon);
			sr.end();
		}
	}
	
	@Override
	public String toString() {
		return "Player: x = " + x + ", y = " + y + ", width = " + width + ", height = " + height;
	}
}
