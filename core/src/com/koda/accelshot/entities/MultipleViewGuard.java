package com.koda.accelshot.entities;

import com.badlogic.gdx.math.MathUtils;
import com.koda.accelshot.handlers.Path;
import com.koda.accelshot.handlers.RotationWaypoint;

public class MultipleViewGuard extends PathGuard {

	private RotationWaypoint[] rw;
	
	public MultipleViewGuard(float x, float y, float width, float height, float startRadians, 
			int direction, float turnRate, int views) {
		this(x, y, width, height, startRadians, new RotationWaypoint[] {new RotationWaypoint(0, direction, turnRate, 0)}, 3);
	}
	
	public MultipleViewGuard(float x, float y, float width, float height, float startRadians, 
			RotationWaypoint[] rw, int views) {
		this(x, y, width, height, startRadians, new Path[] {new Path(x, y, 0, 0, null)}, rw, 0, views);
	}
	
	public MultipleViewGuard(float x, float y, float width, float height, float startRadians, 
			Path[] path, RotationWaypoint[] rw, int pathnum, int views) {
		super(x, y, width, height, startRadians, path, pathnum);
		this.rw = rw;
		this.fieldsOfView = views;
		visionViewx = new float[views + (visionCurveSegments + 1) * views];
		visionViewy = new float[views + (visionCurveSegments + 1) * views];
		visionViewPolygon = new float[visionViewx.length * 2];
	}
	
	@Override
	protected int rotate(float dt) {
		float amount = rw[rotationpathnum].direction * dt * rw[rotationpathnum].turnRate;
		radians += amount;
		
		//rotate indefinitely if length is 1 and distance needed is 0
		if (!(rw.length == 1 && rw[0].distance == 0)) {
			rotationAccum += Math.abs(amount);
			if (rotationAccum >= Math.abs(rw[rotationpathnum].distance)) {
				rotationAccum = 0;
				//the radians will always overshoot, so correct it
				radians = initialRadians + rw[rotationpathnum].distance * rw[rotationpathnum].direction;
				initialRadians = radians;
				//System.out.println("Finished rotating of " + rotationpathnum);
				rotationpathnum = (rotationpathnum + 1) % rw.length;
				rotationDelayTimer = rw[rotationpathnum].delay;
			}
		}
		return 0;
	}
	
	@Override
	protected void updatePath(float dt) {
		if (distanceTraveled >= distanceNeeded) {
			x = path[pathnum].x;
			y = path[pathnum].y;
			distanceTraveled = 0;
			pathnum = (pathnum + 1) % path.length;
			delayTimer = path[pathnum].delay;
			speed = path[pathnum].speed;
			distanceNeeded = distanceTo(path[pathnum].x, path[pathnum].y);
			targetAngle = MathUtils.atan2(path[pathnum].y - y, path[pathnum].x - x);
		}
		
		if (delayTimer == 0) {
			dx = speed * dt * MathUtils.cos(targetAngle);
			dy = speed * dt * MathUtils.sin(targetAngle);
			x += dx;
			y += dy;
			distanceTraveled += Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
		}
	}
	
	/*@Override
	public void updateBody() {
		
		final int MAX = 50;
		//first view
		
		int arc = visionViewx.length / views - 1;
		int index = 0;
		int buff = 0;
		for (int viewnum = 0; viewnum < views; viewnum++) {
			float theta = radians - viewnum * 2*pi / views - visionRange / 2;
			int arcGroup = viewnum * arc;
			for (; index < arcGroup + buff + arc; index++) {
				int iterations = 0;
				do {
					iterations++;
					visionViewx[index] = x + MathUtils.cos(theta) * visionRadius * iterations / MAX;
					visionViewy[index] = y + MathUtils.sin(theta) * visionRadius * iterations / MAX + VISION_SHIFTY;
				} while (iterations < MAX && isVisionOK(index, 0));
				theta += visionRange / visionCurveSegments;
			}
			
			if (index != visionViewx.length) {
				visionViewx[index] = x;
				visionViewy[index] = y + VISION_SHIFTY;
				index++;
				buff++;
			}
		}
		
		float theta = radians;
		for (int i = 0; i < shapex.length; i++) {
			shapex[i] = x + MathUtils.cos(theta) * width / 2;
			shapey[i] = y + MathUtils.sin(theta) * height / 2;
			shapePolygon[i * 2] = shapex[i];
			shapePolygon[i * 2 + 1] = shapey[i];
			theta += 2*pi / bodyCurveSegments;
		}
		
		for (int i = 0; i < visionViewx.length; i++) {
			visionViewPolygon[i * 2] = visionViewx[i];
			visionViewPolygon[i * 2 + 1] = visionViewy[i];
		}
	}*/
	
	@Override
	public void update(float dt) {
		super.update(dt);
		if (rw != null)
			rotate(dt);
	}
}
