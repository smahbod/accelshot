package com.koda.accelshot.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.koda.accelshot.AccelShot;
import com.koda.accelshot.handlers.Constants;
import com.koda.accelshot.handlers.LevelReader;
import com.koda.accelshot.states.State;
import com.koda.accelshot.ui.Animation;


public abstract class Guard extends Entity {
	
	public static final int CLOCKWISE = -1;
	public static final int COUNTER_CLOCKWISE = 1;
	
	protected final int visionCurveSegments = 20;
	protected final int bodyCurveSegments = 15;
	
	protected final float WIDENING_RADIUS_RATE = 20f;
	protected final float WIDENING_RANGE_RATE = .2f;
	protected final float WIDENING_LIMIT = pi/24;
	protected final float VISION_SHIFTY = 8f;
	
	protected float wobbleAccum;
	protected float rotationAccum;
	protected float wideningAccum;
	
	protected boolean widening = true;
	protected boolean rotating;
	protected boolean wobbleMode;
	
	protected int wobbled = 1;
	protected int widened = 1;
	protected int rotationDirection = 0;
	protected int fieldsOfView = 1;
	
	protected float[] visionViewx;
	protected float[] visionViewy;
	protected float[] visionViewPolygon;
	
	protected Animation animation;
	
	protected PolygonSprite poly;
	protected PolygonSpriteBatch polyBatch = new PolygonSpriteBatch();
	protected Texture textureSolid;
	
	//public fields
	public float turnRate = 2;
	public float visionWobble = pi/8;
	public float visionRange = pi/4;
	public float visionRadius = 200;
	public int spinning = 0;
	public boolean wobblingEnabled = false;
	public boolean wideningEnabled = false;
	
	//constructor
	protected Guard(float x, float y, float width, float height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		visionViewx = new float[fieldsOfView + (visionCurveSegments + 1) * fieldsOfView];
		visionViewy = new float[fieldsOfView + (visionCurveSegments + 1) * fieldsOfView];
		visionViewPolygon = new float[visionViewx.length * 2];
		shapex = new float[bodyCurveSegments];
		shapey = new float[bodyCurveSegments];
		shapePolygon = new float[shapex.length * 2];
		
		Texture tex = new Texture(Gdx.files.internal("accelshot_robot1_spritesheet_16_clear.png"));
		TextureRegion[][] frames = TextureRegion.split(tex, AccelShot.TILE_SIZE, AccelShot.TILE_SIZE);
		animation = new Animation(frames, .1875f);
		
		
		
		
		Pixmap pix = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
		pix.setColor(0xDEADBEFF); // DE is red, AD is green and BE is blue.
		pix.fill();
		textureSolid = new Texture(pix);
	}
	
	public float[] getVisionX() { return visionViewx; }
	public float[] getVisionY() {return visionViewy; }

	protected void wobble(float dt) {
		if (!wobblingEnabled || rotating)
			return;
		
		float amount = dt * turnRate / 9;
		
		if (wobbleMode) {
			radians += amount;
		} else {
			radians -= amount;
		}
		
		wobbleAccum += amount;
		if (wobbleAccum >= visionWobble / 2) {
			wobbled++;
			wobbleAccum = 0;
			if (wobbled == 2) {
				wobbled = 0;
				wobbleMode = !wobbleMode;
			}
		}
	}
	
	protected void normalizeRadians() {
		if (rotating)
			return;
		
		while (radians >= 2 * pi) {
			radians -= 2 * pi;
		}
		
		while (radians < 0) {
			radians += 2 * pi;
		}
	}
	
	protected boolean isVisionOK(int i, int pattern) {
		for (CollisionBox cb : LevelReader.getCollisionBoxes()) {
			if (cb.contains(visionViewx[i], visionViewy[i])) {

				Vector2 intersection = new Vector2();
				Vector2 guard = new Vector2(x, y);
				Vector2 finish = new Vector2(visionViewx[i], visionViewy[i]);
				
				for (int j = 0; j < cb.shapex.length; j++) {
					int k = (j + 1) % cb.shapex.length;
					if (Intersector.intersectSegments(guard, finish, 
							new Vector2(cb.shapex[j], cb.shapey[j]), 
							new Vector2(cb.shapex[k], cb.shapey[k]), 
							intersection)) {
						visionViewx[i] = intersection.x;
						visionViewy[i] = intersection.y;
					}
				}

				return false;
			}
		}
		return true;
	}
	
	@Override
	public void updateBody() {
		final int MAX = 50;
		int arc = visionViewx.length / fieldsOfView - 1;
		int index = 0;
		int buff = 0;
		for (int viewnum = 0; viewnum < fieldsOfView; viewnum++) {
			float theta = radians - viewnum * 2*pi / fieldsOfView - visionRange / 2;
			int arcGroup = viewnum * arc;
			for (; index < arcGroup + buff + arc; index++) {
				int iterations = 0;
				do {
					iterations++;
					visionViewx[index] = x + MathUtils.cos(theta) * visionRadius * iterations / MAX;
					visionViewy[index] = y + MathUtils.sin(theta) * visionRadius * iterations / MAX + VISION_SHIFTY;
				} while (iterations < MAX && isVisionOK(index, 0));
				theta += visionRange / visionCurveSegments;
			}
			
			if (index != visionViewx.length) {
				visionViewx[index] = x;
				visionViewy[index] = y + VISION_SHIFTY;
				index++;
				buff++;
			}
		}
		
		float theta = radians;
		for (int i = 0; i < shapex.length; i++) {
			shapex[i] = x + MathUtils.cos(theta) * width / 2;
			shapey[i] = y + MathUtils.sin(theta) * height / 2;
			shapePolygon[i * 2] = shapex[i];
			shapePolygon[i * 2 + 1] = shapey[i];
			theta += 2*pi / bodyCurveSegments;
		}
		
		for (int i = 0; i < visionViewx.length; i++) {
			visionViewPolygon[i * 2] = visionViewx[i];
			visionViewPolygon[i * 2 + 1] = visionViewy[i];
		}
		
		short[] triangulationCoords = new short[(visionViewx.length - 2) * 3];
		for (int i = 0; i < visionViewx.length - 2; i++) {
			triangulationCoords[i * 3] = (short) (visionViewx.length - 1);
			triangulationCoords[i * 3 + 1] = (short) i;
			triangulationCoords[i * 3 + 2] = (short) (i + 1);
		}
		
		PolygonRegion polyReg = new PolygonRegion(new TextureRegion(textureSolid), visionViewPolygon, triangulationCoords);
		poly = new PolygonSprite(polyReg);
	}
	
	protected void updateAnimationAngle() {
		//cardinal directions
		if (radians >= 15*pi/8 || radians < pi/8) {
			animation.setAnimationRow(Constants.ANIMATION_ROBOT_RIGHT);
			animation.setFrame(0);
		} else if (radians >= 3*pi/8 && radians < 5*pi/8) {
			animation.setAnimationRow(Constants.ANIMATION_ROBOT_UP);
			animation.setFrame(0);
		} else if (radians >= 7*pi/8 && radians < 9*pi/8) {
			animation.setAnimationRow(Constants.ANIMATION_ROBOT_LEFT);
			animation.setFrame(0);
		} else if (radians >= 11*pi/8 && radians < 13*pi/8) {
			animation.setAnimationRow(Constants.ANIMATION_ROBOT_DOWN);
			animation.setFrame(0);
		}
		
		//diagonals
		else if (radians >= pi/8 && radians < 3*pi/8) {
			animation.setAnimationRow(Constants.ANIMATION_ROBOT_DIAGONALS);
			animation.setFrame(Constants.ANIMATION_ROBOT_UP_RIGHT);
		} else if (radians >= 5*pi/8 && radians < 7*pi/8) {
			animation.setAnimationRow(Constants.ANIMATION_ROBOT_DIAGONALS);
			animation.setFrame(Constants.ANIMATION_ROBOT_UP_LEFT);
		} else if (radians >= 9*pi/8 && radians < 11*pi/8) {
			animation.setAnimationRow(Constants.ANIMATION_ROBOT_DIAGONALS);
			animation.setFrame(Constants.ANIMATION_ROBOT_DOWN_LEFT);
		} else if (radians >= 13*pi/8 && radians < 15*pi/8) {
			animation.setAnimationRow(Constants.ANIMATION_ROBOT_DIAGONALS);
			animation.setFrame(Constants.ANIMATION_ROBOT_DOWN_RIGHT);
		}
	}
	
	protected void widen(float dt) {
		if (wideningEnabled) {
			int widen = 0;
			wideningAccum += dt * WIDENING_RANGE_RATE;
			if (widening)
				widen = 1;
			else
				widen = -1;
			visionRadius += widen * dt * WIDENING_RADIUS_RATE;
			visionRange += widen * dt * WIDENING_RANGE_RATE;
			if (wideningAccum >= WIDENING_LIMIT) {
				wideningAccum = 0;
				widened++;
				if (widened == 2) {
					widened = 0;
					widening = !widening;
				}
			}
		}
	}
	
	@Override
	public void update(float dt) {
		if (spinning != 0) {
			radians += spinning * dt * .8f;
		}
		
		widen(dt);
		wobble(dt);
		normalizeRadians();
		animation.update(dt);
	}
	
	@Override
	public void render(SpriteBatch sb, ShapeRenderer sr) {
		sb.setProjectionMatrix(State.camera.combined);
		sb.begin();
		TextureRegion image = animation.getFrame();
		float x =  this.x - image.getRegionWidth() / 2;
		float y =  this.y - image.getRegionHeight() / 2;
		sb.draw(image, x, y, image.getRegionWidth() / 2, image.getRegionHeight() / 2, 
				image.getRegionWidth(), image.getRegionHeight(), AccelShot.SCALE, AccelShot.SCALE, 0f);
		sb.end();
		
		
		if (AccelShot.DEBUG) {
			Gdx.gl.glEnable(GL30.GL_BLEND);
			Gdx.gl.glBlendFunc(GL30.GL_SRC_ALPHA, GL30.GL_ONE_MINUS_SRC_ALPHA);
			sr.setColor(1, 1, 0, .5f);
			sr.identity();
			sr.setProjectionMatrix(State.camera.combined);
			sr.begin(ShapeType.Line);
			sr.polygon(shapePolygon);
			sr.polygon(visionViewPolygon);
			sr.end();
			Gdx.gl.glDisable(GL30.GL_BLEND);
		}	
	}
	
	public void renderVision() {
		if (poly != null) {
			polyBatch.setProjectionMatrix(State.camera.combined);
			poly.setColor(1, 1, 1, .5f);
			polyBatch.begin();
			poly.draw(polyBatch);
			polyBatch.end();
		}
	}
	
	@Override
	public String toString() {
		return "Guard: x = " + x + ", y = " + y + ", width = " + width + ", height = " + height;
	}
}
