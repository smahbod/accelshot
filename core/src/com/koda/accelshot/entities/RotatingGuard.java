package com.koda.accelshot.entities;

import com.koda.accelshot.handlers.Path;
import com.koda.accelshot.handlers.RotationWaypoint;

public class RotatingGuard extends PathGuard {
	
	public RotatingGuard(float x, float y, float width, float height, float startRadians, RotationWaypoint[] rw) {
		super(x, y, width, height, startRadians, 
				new Path[] {new Path(x, y, 0, 0, rw == null ? defaultRotationWaypoint() : rw)}, 0);
	}
	
	public RotatingGuard(float x, float y, float width, float height, float startRadians, int dir, float turnRate) {
		this(x, y, width, height, startRadians, defaultRotationWaypoint(0, dir, turnRate, 0));
	}
	
	private static RotationWaypoint[] defaultRotationWaypoint() {
		return new RotationWaypoint[] {new RotationWaypoint(0, COUNTER_CLOCKWISE, 1f, 0)};
	}
	
	private static RotationWaypoint[] defaultRotationWaypoint(float distance, int direction, float turnRate, float delay) {
		return new RotationWaypoint[] {new RotationWaypoint(distance, direction, turnRate, delay)};
	}
	
	@Override
	public String toString() {
		return "Rotating" + super.toString();
	}
}
