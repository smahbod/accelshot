package com.koda.accelshot.ui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.koda.accelshot.AccelShot;
import com.koda.accelshot.handlers.Calculations;
import com.koda.accelshot.states.State;

public class Clock {

	public float x;
	public float y;
	
	private float targetX;
	private float targetY;
	private float distanceNeeded;
	private float clock;
	private boolean running;
	private float incrementTimer;
	private float timer;
	private float maxTime = 1f;
	
	public Clock(int startingTime, float x, float y) {
		this.clock = startingTime;
		this.x = x;
		this.y = y;
	}
	
	public Clock(int startingTime, float x, float y, float targetX, float targetY) {
		this(startingTime, x, y);
		this.targetX = targetX;
		this.targetY = targetY;
		distanceNeeded = Calculations.distanceBetween(x, y, targetX, targetY);
	}
	
	public void setMaxTime(float f) {
		maxTime = f;
	}
	
	public void start() {
		running = true;
	}
	
	public void stop() {
		running = false;
	}
	
	public int getTime() {
		return (int) clock;
	}
	
	public float getFloatTime() {
		return clock;
	}
	
	public void setTime(float t) {
		if (t < 0f)
			t = 0f;
		
		clock = t;
	}
	
	public void setTarget(float tx, float ty) {
		targetX = tx;
		targetY = ty;
		distanceNeeded = Calculations.distanceBetween(x, y, tx, ty);
	}
	
	public void update(float dt) {
		if (!running) {
			if (timer < maxTime) {
				timer += dt;
				if (timer >= maxTime) {
					timer = maxTime;
				}
				
				float radians = MathUtils.atan2(targetY - y, targetX - x);
				x += MathUtils.cos(radians) * distanceNeeded * dt / maxTime;
				y += MathUtils.sin(radians) * distanceNeeded * dt / maxTime;
				
			}
			
			if (timer == maxTime && (x != targetX || y != targetY)) {
				x = targetX;
				y = targetY;
			}
		}
	
		if (running) {
			incrementTimer += dt;
			if (incrementTimer >= 1f) {
				incrementTimer = 0;
				clock++;
			}
		}
	}
	
	public void render(SpriteBatch sb) {
		sb.setProjectionMatrix(State.camera.combined);
		sb.begin();
		BitmapFont time = AccelShot.etext.getTimeFont();
		float posx = x - time.getBounds("W:WW").width / 2;
		float posy = y + time.getBounds("W:WW").height / 2;
		String minutes = Integer.toString((int) clock / 60);
		String seconds = Integer.toString((int) clock % 60);
		if (seconds.equals("0")) {
			seconds += "0";
		}
		if (Integer.parseInt(seconds) < 10 && Integer.parseInt(seconds) != 0) {
			seconds = "0" + seconds;
		}
		time.draw(sb, minutes + ":" + seconds, posx, posy);
		sb.end();
	}
}
