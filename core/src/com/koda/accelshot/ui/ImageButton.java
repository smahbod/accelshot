package com.koda.accelshot.ui;

import com.badlogic.gdx.graphics.g2d.Sprite;

public class ImageButton extends Button {

	private Sprite image;
	
	public ImageButton(Sprite image, int id, float x, float y, float width, float height) {
		super(id, x, y, width, height);
		this.image = image;
	}
	
	@Override
	public void setPressed() {
		super.setPressed();
		image.setX(x - image.getWidth() / 2);
		image.setY(y - image.getHeight() / 2);
	}
	
	@Override
	public void setUnpressed() {
		super.setUnpressed();
		image.setX(x - image.getWidth() / 2);
		image.setY(y - image.getHeight() / 2);
	}
	
	public Sprite getImage() {
		return image;
	}
}