package com.koda.accelshot.ui;


public class TextButton extends Button {

	private String text;
	
	public TextButton(String text, int id, float x, float y, float width, float height) {
		super(id, x, y, width, height);
		this.text = text;
	}
	
	public String getText() {
		return text;
	}
}

