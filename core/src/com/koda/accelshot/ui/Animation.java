package com.koda.accelshot.ui;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Animation {

	private TextureRegion[][] frames;
	private float timer;
	private float frameTimer = .25f;
	public int current;
	public int row;
	private boolean animating;
	
	public Animation(TextureRegion[][] frames, float frameTimer) {
		this.frames = frames;
		this.frameTimer = frameTimer;
	}
	
	public void setAnimationRow(int r) {
		row = r;
	}
	
	public void start() {
		animating = true;
	}
	
	public void stop() {
		animating = false;
	}
	
	public void setFrame(int f) {
		current = f;
	}
	
	public void resetFrame() {
		setFrame(0);
	}
	
	public TextureRegion getFrame() {
		return frames[row][current];
	}
	
	public void update(float dt) {
		if (animating) {
			timer += dt;
			if (timer >= frameTimer) {
				timer = 0;
				current = (current + 1) % frames[row].length;
			}
		}
	}
}
