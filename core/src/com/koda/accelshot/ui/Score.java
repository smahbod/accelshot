package com.koda.accelshot.ui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.koda.accelshot.AccelShot;
import com.koda.accelshot.handlers.Calculations;
import com.koda.accelshot.handlers.Constants;
import com.koda.accelshot.handlers.Save;
import com.koda.accelshot.states.State;

public class Score {

	public float x;
	public float y;
	public float targetX;
	public float targetY;
	
	private float distanceNeeded;
	//private final float rate = 120f;
	private final float bulletScoreRate = 20;
	private final float timeScoreRate = 60f;
	private final int GOING_RED = 1;
	private final int GOING_YELLOW = 0;
	private int currentColor;
	private int bucket;
	private int recordTime;
	private int recordBullets;
	private float timeBonus;
	private float bulletBonus;
	private float score;
	private float timer;
	private float maxTime = 1f;
	private float colorTimer;
	private float maxColorTime = .5f;
	private boolean isHighScore;
	
	public Score(int bucket, float x, float y, float targetX, float targetY) {
		this.score = 0;
		this.bucket = bucket;
		this.x = x;
		this.y = y;
		this.targetX = targetX;
		this.targetY = targetY;
		distanceNeeded = Calculations.distanceBetween(x, y, targetX, targetY);
	}
	
	public void skip() {
		if (bulletBonus > 0) {
			score += bulletBonus;
			bulletBonus = 0;
		} else if (timeBonus > 0) {
			score += timeBonus;
			timeBonus = 0;
		}
	}
	
	public void setMaxTime(float f) {
		maxTime = f;
	}
	
	public void addScore(float s) {
		bucket += s;
	}
	
	public void setTimeBonus(int b) {
		timeBonus = b;
		recordTime = b / Constants.TIME_SCORE_MULTIPLIER;
	}
	
	public void setBulletBonus(int b) {
		bulletBonus = b;
		recordBullets = b / Constants.BULLET_SCORE_MULTIPLIER;
	}
	
	public void checkHighScore() {
		isHighScore = Save.gd.addEntry(State.getLevel(), Constants.TIME_SCORE_THRESHOLD - recordTime, Constants.BULLET_SCORE_THRESHOLD - recordBullets + 1, bucket);
		if (isHighScore)
			Save.save();
	}
	
	public int getScore() {
		return  bucket;
	}
	
	public String formatScoreAsString() {
		return Integer.toString((int) score);
	}
	
	public void update(float dt) {
		if (colorTimer < maxColorTime) {
			colorTimer += dt;
			if (colorTimer >= maxColorTime) {
				colorTimer = 0;
				currentColor = (currentColor + 1) % 2;
			}
		}
		
		if (timer < maxTime) {
			timer += dt;
			if (timer >= maxTime) {
				timer = maxTime;
			}
			
			float radians = MathUtils.atan2(targetY - y, targetX - x);
			x += MathUtils.cos(radians) * distanceNeeded * dt / maxTime;
			y += MathUtils.sin(radians) * distanceNeeded * dt / maxTime;
			
		}
		
		if (timer == maxTime && (x != targetX || y != targetY)) {
			x = targetX;
			y = targetY;
		}
		
		if (timer == maxTime && score < bucket) {
			//score += dt * rate;
			if (bulletBonus > 0) {
				score += dt * bulletScoreRate;
				bulletBonus -= dt * bulletScoreRate;
				if (bulletBonus < 0) {
					bulletBonus = 0;
				}
			}
			
			if (timeBonus > 0 && bulletBonus == 0) {
				score += dt * timeScoreRate;
				timeBonus -= dt * timeScoreRate;
				if (timeBonus < 0) {
					timeBonus = 0;
				}
			}
			
			if (timeBonus == 0 && bulletBonus == 0 && score != bucket) {
				score = bucket;
			}
		}
	}
	
	public void render(SpriteBatch sb) {
		sb.setProjectionMatrix(State.camera.combined);
		sb.begin();
		BitmapFont scoretext = AccelShot.etext.getScoreFont();
		scoretext.setColor(1, 1, 1, 1);
		float posx = x - scoretext.getBounds(formatScoreAsString()).width / 2;
		float posy = y + scoretext.getBounds(formatScoreAsString()).height / 2;
		scoretext.draw(sb, formatScoreAsString(), posx, posy);
		
		//bullet bonus
		String bulletbonus = "Bullet bonus: ";
		posx = x - scoretext.getBounds(bulletbonus + Calculations.getDigits(10)).width / 2;
		posy = y + scoretext.getBounds(bulletbonus + Calculations.getDigits(10)).height / 2 - 50;
		scoretext.draw(sb, bulletbonus + (int) bulletBonus, posx, posy);
				
		//time bonus
		String timebonus = "Time bonus: ";
		posx = x - scoretext.getBounds(timebonus + Calculations.getDigits(100)).width / 2;
		posy = y + scoretext.getBounds(timebonus + Calculations.getDigits(100)).height / 2 - 100;
		scoretext.draw(sb, timebonus + (int) timeBonus, posx, posy);
		
		if (timeBonus == 0 && bulletBonus == 0 && isHighScore) {
			//new high score! yay
			String highscore = "New high score!";
			posx = x - scoretext.getBounds(highscore).width / 2;
			posy = y + scoretext.getBounds(highscore).height / 2 + 50;
			if (currentColor == GOING_RED) {
				scoretext.setColor(1, 1 - colorTimer / maxColorTime, 0, 1);
			} else {
				scoretext.setColor(1, colorTimer / maxColorTime, 0, 1);
			}
			scoretext.draw(sb, highscore, posx, posy);
		}
		
		sb.end();
	}
}
