package com.koda.accelshot.ui;

import com.badlogic.gdx.graphics.Color;
import com.koda.accelshot.AccelShot;

public abstract class Button {

	private final float PRESS_DISPLACEMENT = AccelShot.TILE_SIZE * 1 / 16 * AccelShot.SCALE;
	public float x;
	public float y;
	public float width;
	public float height;
	public com.badlogic.gdx.graphics.Color color;
	protected int id;
	protected boolean previouslyPressed;
	protected boolean canBePressed = true;
	
	public Button(int id, float x, float y, float width, float height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.id = id;
		color = new Color(1, 1, 1, 1);
	}
	
	public boolean canBePressed() {
		return canBePressed;
	}
	
	public void setCanBePressed(boolean b) {
		canBePressed = b;
	}
	
	public void setPressed() {
		if (!previouslyPressed) {
			this.x -= PRESS_DISPLACEMENT;
			this.y -= PRESS_DISPLACEMENT;
			previouslyPressed = true;
		}
	}
	
	public void setUnpressed() {
		if (previouslyPressed) {
			this.x += PRESS_DISPLACEMENT;
			this.y += PRESS_DISPLACEMENT;
			previouslyPressed = false;
		}
	}
	
	public boolean contains(float x, float y) {
		return x >= this.x - width / 2
				&& y >= this.y - height / 2
				&& x <= this.x + width / 2
				&& y <= this.y + height / 2;
	}
	
	public int getID() {
		return id;
	}
}