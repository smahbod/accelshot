package com.koda.accelshot.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.koda.accelshot.AccelShot;
import com.koda.accelshot.handlers.MovingCamera;

public abstract class State {

	protected GSM gsm;
	protected Vector3 mouse;
	protected State runningState;
	protected static int level;
	public static MovingCamera camera = new MovingCamera();
	static {
		//System.out.println("State says WIDTH = " + AccelShot.WIDTH + " and HEIGHT is " + AccelShot.HEIGHT);
		//camera.setToOrtho(false, AccelShot.WIDTH, AccelShot.HEIGHT);
	}
	
	public State(GSM gsm) {
		this.gsm = gsm;
		mouse = new Vector3();
	}
	
	public void init() {}
	
	public static int getLevel() { return level; }
	
	public State getRunningPlayState() { return runningState; }
	
	protected void drawUI() {}
	
	public void updateScale(float oldScale) {
		camera.setToOrtho(false, AccelShot.WIDTH, AccelShot.HEIGHT);
		drawUI(); 
	}
	
	public abstract void handleInput();
	public abstract void update(float dt);
	public abstract void render(SpriteBatch sb);
}
