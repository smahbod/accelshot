package com.koda.accelshot.states;

import java.util.Stack;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GSM {

	public Stack<State> states;
	
	public GSM() {
		states = new Stack<State>();
	}
	
	public void push(State s) {
		states.push(s);
	}
	
	public State pop() {
		return states.pop();
	}
	
	public State peek() {
		return states.peek();
	}
	
	public void set(State s) {
		pop();
		push(s);
	}
	
	public void handleInput() {
		peek().handleInput();
	}
	
	public void update(float dt) {
		peek().update(dt);
	}
	
	public void render(SpriteBatch sb) {
		peek().render(sb);
	}
}