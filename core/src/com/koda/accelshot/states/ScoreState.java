package com.koda.accelshot.states;

import static com.koda.accelshot.handlers.Constants.BUTTON_MAIN_MENU;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.koda.accelshot.AccelShot;
import com.koda.accelshot.handlers.GameData;
import com.koda.accelshot.handlers.Save;
import com.koda.accelshot.transitions.FadeTransition;
import com.koda.accelshot.ui.TextButton;

public class ScoreState extends State {

	private final String MAIN_MENU = "Main Menu";
	private Array<TextButton> buttons;
	private BitmapFont font;
	private int[] times;
	private int[] bulletsFired;
	private int[] scores;
	
	private final String scoretitle = "Best Scores";
	private final float SEPARATION = 225f;
	
	public ScoreState(GSM gsm) {
		super(gsm);
		times = Save.gd.getTimes();
		bulletsFired = Save.gd.getBulletsFired();
		scores = Save.gd.getScores();

		font = AccelShot.etext.getMenuFont();
		
		buttons = new Array<TextButton>();
		buttons.add(new TextButton(MAIN_MENU, BUTTON_MAIN_MENU, 
				AccelShot.WIDTH / 2, 
				1 * AccelShot.TILE_SIZE * AccelShot.SCALE, 
				font.getBounds(MAIN_MENU).width * 1.5f, 
				font.getBounds(MAIN_MENU).height * 2f));
	}
	
	@Override
	protected void drawUI() {}
	
	private String getFormattedTime(int time) {
		String result = "";
		int minutes = time / 60;
		int seconds = time % 60;
		result += minutes;
		result += ":";
		if (seconds < 10)
			result += "0";
		result += seconds;
		return result;
	}

	@Override
	public void handleInput() {
		mouse.x = Gdx.input.getX();
		mouse.y = Gdx.input.getY();
		camera.unproject(mouse);
		for (TextButton tb : buttons) {
			if (tb.contains(mouse.x, mouse.y)) {
				tb.color.set(1, 0, 0, 1);
			} else {
				tb.color.set(1, 1, 1, 1);
			}
		}
		
		if (Gdx.input.justTouched()) {
			for (TextButton tb : buttons) {
				if (tb.contains(mouse.x, mouse.y)) {
					switch (tb.getID()) {
					case BUTTON_MAIN_MENU:
						gsm.set(new TransitionState(gsm, this, new MenuState(gsm), new FadeTransition()));
						break;
					}
				}
			}		
		}
		
		/*if (Gdx.input.isKeyJustPressed(Keys.ANY_KEY)) {
			gsm.set(new TransitionState(gsm, this, new MenuState(gsm), new FadeTransition()));
		}*/
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
	}

	@Override
	public void render(SpriteBatch sb) {
		sb.setProjectionMatrix(camera.combined);
		sb.begin();
		BitmapFont titletext = AccelShot.etext.getTitleFont();
		BitmapFont scoretext = AccelShot.etext.getScoreFont();
		titletext.setColor(1, 1, 1, 1);
		scoretext.setColor(1, 1, 1, 1);
		float posx = AccelShot.WIDTH / 2 - titletext.getBounds(scoretitle).width / 2;
		float posy = AccelShot.HEIGHT * 4 / 5 + titletext.getBounds(scoretitle).height / 2;
		titletext.draw(sb, scoretitle, posx, posy);
		
		posx = AccelShot.WIDTH / 6;
		posy = AccelShot.HEIGHT * 4 / 5 - 50 ;
		String text = "Level";
		scoretext.draw(sb, text, 
				posx - scoretext.getBounds(text).width / 2, 
				posy + scoretext.getBounds(text).height / 2);
		
		text = "Bullets Fired";
		posx += SEPARATION;
		scoretext.draw(sb, text, 
				posx - scoretext.getBounds(text).width / 2, 
				posy + scoretext.getBounds(text).height / 2);
		
		text = "Time Taken";
		posx += SEPARATION;
		scoretext.draw(sb, text, 
				posx - scoretext.getBounds(text).width / 2, 
				posy + scoretext.getBounds(text).height / 2);
		
		text = "Total";
		posx += SEPARATION;
		scoretext.draw(sb, text, 
				posx - scoretext.getBounds(text).width / 2, 
				posy + scoretext.getBounds(text).height / 2);
		
		float offsetY = 100;
		for (int i = 0; i < GameData.LEVELS; i++) {
			posx = AccelShot.WIDTH / 6;
			posy = AccelShot.HEIGHT * 4 / 5 - offsetY;
			scoretext.draw(sb, Integer.toString(i + 1), 
					posx - scoretext.getBounds(Integer.toString(i + 1)).width / 2, 
					posy + scoretext.getBounds(Integer.toString(i + 1)).height / 2);
			
			String bullets = Integer.toString(bulletsFired[i]);
			if (bulletsFired[i] == -1)
				bullets = "---";
			posx += SEPARATION;
			scoretext.draw(sb, bullets, 
					posx - scoretext.getBounds(bullets).width / 2, 
					posy + scoretext.getBounds(bullets).height / 2);
			
			String time = getFormattedTime(times[i]);
			if (times[i] == -1)
				time = "---";
			posx += SEPARATION;
			scoretext.draw(sb, time, 
					posx - scoretext.getBounds(time).width / 2, 
					posy + scoretext.getBounds(time).height / 2);
			
			String total = Integer.toString(scores[i]);
			if (time.equals("---"))
				total = "---";
			posx += SEPARATION;
			scoretext.draw(sb, total, 
					posx - scoretext.getBounds(total).width / 2, 
					posy + scoretext.getBounds(total).height / 2);
			
			offsetY += 50;
		}
		
		sb.end();
		
		sb.begin();
		for (TextButton tb : buttons) {
			font.setColor(tb.color);
			font.draw(sb, tb.getText(),
					tb.x - font.getBounds(tb.getText()).width / 2, 
					tb.y + font.getBounds(tb.getText()).height / 2);
		}
		sb.end();
	}
}
