package com.koda.accelshot.states;

import static com.koda.accelshot.handlers.Constants.BUTTON_MAIN_MENU;
import static com.koda.accelshot.handlers.Constants.BUTTON_RESUME;
import static com.koda.accelshot.handlers.Constants.BUTTON_RETRY;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.koda.accelshot.AccelShot;
import com.koda.accelshot.handlers.LevelReader;
import com.koda.accelshot.handlers.LoseCondition;
import com.koda.accelshot.transitions.FadeTransition;
import com.koda.accelshot.ui.TextButton;

public class DefeatState extends State {

	private final String RETRY = "Retry";
	private final String MAIN_MENU = "Main Menu";
	private Array<TextButton> buttons;
	private BitmapFont font;
	private State display;
	private TextureRegion texture;
	private String message;
	private float timer;
	private float maxTime = 1f;
	
	public DefeatState(GSM gsm, State display, float maxTime, LoseCondition condition) {
		super(gsm);
		this.display = display;
		this.maxTime = maxTime;
		this.message = condition.message;
		if (condition.reason == LoseCondition.REASON_SPOTTED) {
			//texture = AccelShot.res.getAtlas("pack").findRegion("red");
			texture = new TextureRegion(new Texture(Gdx.files.internal("red.png")));
		} else {
			texture = AccelShot.res.getAtlas("pack").findRegion("black");
		}
		
		font = AccelShot.etext.getMenuFont();	
		buttons = new Array<TextButton>();
	}
	
	@Override
	protected void drawUI() {
		buttons.add(new TextButton(RETRY, BUTTON_RETRY, 
				AccelShot.WIDTH / 2, 
				AccelShot.HEIGHT / 2 - 1.5f * AccelShot.TILE_SIZE * AccelShot.SCALE, 
				font.getBounds(RETRY).width * 1.5f, 
				font.getBounds(RETRY).height * 2f));
		
		buttons.add(new TextButton(MAIN_MENU, BUTTON_MAIN_MENU, 
				AccelShot.WIDTH / 2, 
				AccelShot.HEIGHT / 2 - 2.5f * AccelShot.TILE_SIZE * AccelShot.SCALE, 
				font.getBounds(MAIN_MENU).width * 1.5f, 
				font.getBounds(MAIN_MENU).height * 2f));
	}

	@Override
	public void handleInput() {
		mouse.x = Gdx.input.getX();
		mouse.y = Gdx.input.getY();
		camera.unproject(mouse);
		for (TextButton tb : buttons) {
			if (tb.contains(mouse.x, mouse.y)) {
				tb.color.set(1, 0, 0, 1);
			} else {
				tb.color.set(1, 1, 1, 1);
			}
		}
		
		if (Gdx.input.justTouched()) {
			for (TextButton tb : buttons) {
				if (tb.contains(mouse.x, mouse.y)) {
					switch (tb.getID()) {
					case BUTTON_RETRY:
						gsm.set(new TransitionState(gsm, this, new PlayState(gsm, LevelReader.getLevel()), new FadeTransition()));
						break;
					case BUTTON_MAIN_MENU:
						gsm.set(new TransitionState(gsm, this, new MenuState(gsm), new FadeTransition()));
						break;
					}
				}
			}		
		}
	}

	@Override
	public void update(float dt) {
		if (timer < maxTime) {
			timer += dt;
			if (timer >= maxTime) {
				timer = maxTime;
				drawUI();
			}
		}
		display.update(dt);
	}

	@Override
	public void render(SpriteBatch sb) {
		display.render(sb);
		
		sb.setProjectionMatrix(camera.combined);
		sb.begin();
		float alpha = (timer / maxTime) / 1.5f;
		sb.setColor(1f, 1f, 1f, alpha);
		Sprite sprite = new Sprite(texture);
		sb.draw(sprite, 0, 0, AccelShot.WIDTH, AccelShot.HEIGHT);
		BitmapFont postgame = AccelShot.etext.getPostGameFont();
		postgame.setColor(1, 1, 1, alpha * 1.25f);
		float pos = AccelShot.WIDTH / 2 - postgame.getBounds(message).width / 2;
		postgame.draw(sb, message, pos, AccelShot.HEIGHT / 2);
		sb.end();
		
		if (timer == maxTime) {
			sb.begin();
			for (TextButton tb : buttons) {
				font.setColor(tb.color);
				font.draw(sb, tb.getText(),
						tb.x - font.getBounds(tb.getText()).width / 2, 
						tb.y + font.getBounds(tb.getText()).height / 2);
			}
			sb.end();
		}
	}
}
