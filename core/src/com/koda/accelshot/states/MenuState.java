package com.koda.accelshot.states;

import static com.koda.accelshot.handlers.Constants.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.Array;
import com.koda.accelshot.AccelShot;
import com.koda.accelshot.handlers.Constants;
import com.koda.accelshot.transitions.FadeTransition;
import com.koda.accelshot.ui.TextButton;

public class MenuState extends State {
	
	private final String LEVEL_SELECT = "Level Select";
	private final String SCORES = "Scores";
	private final String EXIT = "Quit";
	private Array<TextButton> buttons;
	
	private float offsetX;
	
	public MenuState(GSM gsm) {
		super(gsm);
		
		buttons = new Array<TextButton>();
		
		BitmapFont font = AccelShot.etext.getMenuFont();
		buttons.add(new TextButton(LEVEL_SELECT, BUTTON_LEVEL_SELECT, 
				AccelShot.WIDTH / 2, 
				AccelShot.HEIGHT / 2, 
				font.getBounds(LEVEL_SELECT).width * 1.5f, 
				font.getBounds(LEVEL_SELECT).height * 2f));
		
		buttons.add(new TextButton(SCORES, BUTTON_SCORES, 
				AccelShot.WIDTH / 2, 
				AccelShot.HEIGHT / 2 - 1 * AccelShot.TILE_SIZE * AccelShot.SCALE, 
				font.getBounds(SCORES).width * 1.5f, 
				font.getBounds(SCORES).height * 2f));
		
		buttons.add(new TextButton(EXIT, BUTTON_EXIT, 
				AccelShot.WIDTH / 2, 
				AccelShot.HEIGHT / 2 - 2 * AccelShot.TILE_SIZE * AccelShot.SCALE, 
				font.getBounds(EXIT).width * 1.5f, 
				font.getBounds(EXIT).height * 2f));
		
		offsetX = AccelShot.etext.getTitleFont().getBounds(AccelShot.TITLE).width / 2;
	}
	
	@Override
	protected void drawUI() {}

	@Override
	public void handleInput() {
		mouse.x = Gdx.input.getX();
		mouse.y = Gdx.input.getY();
		camera.unproject(mouse);
		for (TextButton tb : buttons) {
			if (tb.contains(mouse.x, mouse.y)) {
				tb.color.set(1, 0, 0, 1);
			} else {
				tb.color.set(1, 1, 1, 1);
			}
		}
		
		if (Gdx.input.justTouched()) {
			for (TextButton tb : buttons) {
				if (tb.contains(mouse.x, mouse.y)) {
					switch (tb.getID()) {
					case BUTTON_LEVEL_SELECT:
						gsm.set(new TransitionState(gsm, this, new LevelSelectState(gsm, 1), new FadeTransition()));
						break;
					case BUTTON_SCORES:
						gsm.set(new TransitionState(gsm, this, new ScoreState(gsm), new FadeTransition()));
						break;
					case BUTTON_EXIT:
						Gdx.app.exit();
						break;
					}
				}
			}		
		}
		
		if (Gdx.input.isKeyJustPressed(Keys.L)) {
			gsm.set(new TransitionState(gsm, this, new LevelSelectState(gsm, 1), new FadeTransition()));
		}
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(SpriteBatch sb) {
		//TextButton tb1 = null;
		sb.setProjectionMatrix(camera.combined);
		sb.begin();
		AccelShot.etext.getTitleFont().draw(sb, AccelShot.TITLE, AccelShot.WIDTH / 2 - offsetX, AccelShot.HEIGHT * 4 / 5);
		for (TextButton tb : buttons) {
			//tb1 = tb;
			AccelShot.etext.getMenuFont().setColor(tb.color);
			AccelShot.etext.getMenuFont().draw(sb, tb.getText(), 
					tb.x - AccelShot.etext.getMenuFont().getBounds(tb.getText()).width / 2, 
					tb.y + AccelShot.etext.getMenuFont().getBounds(tb.getText()).height / 2);
			
		}
		sb.end();
		
		if (AccelShot.DEBUG) {
			ShapeRenderer sr = new ShapeRenderer();
			sr.begin(ShapeType.Line);
			//sr.rect(tb1.x - tb1.width / 2, tb1.y - tb1.height / 2, tb1.width, tb1.height);
			sr.end();
		}
	}

}
