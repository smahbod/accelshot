package com.koda.accelshot.states;

import static com.koda.accelshot.AccelShot.TILE_SIZE;
import static com.koda.accelshot.AccelShot.SCALE;
import static com.koda.accelshot.AccelShot.WIDTH;
import static com.koda.accelshot.AccelShot.HEIGHT;
import static com.koda.accelshot.entities.Entity.pi;
import static com.koda.accelshot.entities.Guard.CLOCKWISE;
import static com.koda.accelshot.entities.Guard.COUNTER_CLOCKWISE;

import java.math.BigDecimal;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.koda.accelshot.AccelShot;
import com.koda.accelshot.entities.Bullet;
import com.koda.accelshot.entities.CollisionBox;
import com.koda.accelshot.entities.Entity;
import com.koda.accelshot.entities.Guard;
import com.koda.accelshot.entities.PathGuard;
import com.koda.accelshot.entities.Player;
import com.koda.accelshot.entities.RotatingGuard;
import com.koda.accelshot.entities.SimpleGuard;
import com.koda.accelshot.handlers.Calculations;
import com.koda.accelshot.handlers.Constants;
import com.koda.accelshot.handlers.LevelReader;
import com.koda.accelshot.handlers.LoseCondition;
import com.koda.accelshot.handlers.Path;
import com.koda.accelshot.handlers.RotationWaypoint;
import com.koda.accelshot.transitions.FadeTransition;
import com.koda.accelshot.ui.Clock;

public class PlayState extends State {

	private final boolean SANDBOX = false;
	private Array<Entity> entities;
	private Player player;
	private Array<Guard> guards;
	private Array<Bullet> bullets;
	private Array<CollisionBox> collisionBoxes;
	private ShapeRenderer sr;
	private Clock clock;
	
	private float shotTimer;
	private float shotTimerStart = 5f;
	private boolean guardHit = false;
	private boolean defeated;
	
	private final float defaultShotScaleX = 1f;
	
	public PlayState(GSM gsm, int level) {
		super(gsm);
		State.level = level;
		LevelReader.load(level);
		
		bullets = new Array<Bullet>();
		entities = new Array<Entity>();
		player = LevelReader.getPlayer();
		player.setBullets(bullets);
		collisionBoxes = LevelReader.getCollisionBoxes();
		guards = LevelReader.getGuards();
		//guards.add(new RotatingGuard(200, 200, Entity.STD_WIDTH, Entity.STD_HEIGHT, 0, COUNTER_CLOCKWISE, 2));
		entities.addAll(collisionBoxes);
		entities.addAll(guards);
		entities.addAll(bullets);
		entities.add(player);
		sr = new ShapeRenderer();
		
		clock = new Clock(0, AccelShot.WIDTH - 50, 40);
		clock.setTime(45);
		clock.start();
	}
	
	public void init() {
		LevelReader.initializeRenderer(level, SCALE);
		camera.setToOrtho(false, AccelShot.WIDTH, AccelShot.HEIGHT);
	}
	
	private void checkCollisions() {
		//player steps inside vision radius
		if (!SANDBOX && player != null) {
			for (int i = 0; i < guards.size; i++) {
				Guard g = guards.get(i);
				if (player.isCollidingWith(g.getBodyX(), g.getBodyY())
						|| player.isCollidingWith(g.getVisionX(), g.getVisionY())) {
					player.setDead();
					guardHit = false;
					LoseCondition cond = new LoseCondition("You got caught!", LoseCondition.REASON_SPOTTED);
					gsm.set(new DefeatState(gsm, this, 1f, cond));
				}
			}
		}
		
		//bullets-guards
		for (int i = 0; i < bullets.size; i++) {
			for (int j = 0; j < guards.size; j++) {
				Bullet b = bullets.get(i);
				Guard g = guards.get(j);
				if (!b.shouldRemove() && !g.shouldRemove() && g.isCollidingWith(b)) {
					if (!guardHit) {
						guardHit = true;
						shotTimer = shotTimerStart;
					}
					b.setRemove(true);
					g.setRemove(true);
				}
			}
		}
		
		//bullets-collisionboxes
		for (int i = 0; i < bullets.size; i++) {
			for (int j = 0; j < collisionBoxes.size; j++) {
				Bullet b = bullets.get(i);
				CollisionBox cb = collisionBoxes.get(j);
				if (!b.shouldRemove() && cb.isCollidingWith(b)) {
					b.setRemove(true);
				}
			}
		}
	}
	
	@Override
	public void updateScale(float oldScale) {
		super.updateScale(oldScale);
		LevelReader.initializeRenderer(LevelReader.getLevel(), SCALE);
		for (CollisionBox cb : collisionBoxes) {
			float col = (cb.x - TILE_SIZE / 2 * oldScale) / (TILE_SIZE / 2 * oldScale);
			float row = (cb.y - TILE_SIZE / 2 * oldScale) / (TILE_SIZE / 2 * oldScale);
			cb.x = (col * TILE_SIZE / 2 * SCALE) + TILE_SIZE / 2 * SCALE;
			cb.y = (row * TILE_SIZE / 2 * SCALE) + TILE_SIZE / 2 * SCALE;
			cb.width = (cb.width / oldScale) * SCALE;
			cb.height = (cb.height / oldScale) * SCALE;
			cb.updateBody();
		}
		
		float px = (player.x - TILE_SIZE / 2 * oldScale) / oldScale;
		float py = (player.y - TILE_SIZE / 2 * oldScale) / oldScale;
		player.x = px * SCALE + TILE_SIZE / 2 * SCALE;
		player.y = py * SCALE + TILE_SIZE / 2 * SCALE;
		player.width = player.width / oldScale * SCALE;
		player.height = player.height / oldScale * SCALE;
		player.updateBody();
		
		for (Bullet b : bullets) {
			float bx = (b.x - TILE_SIZE / 2 * oldScale) / oldScale;
			float by = (b.y - TILE_SIZE / 2 * oldScale) / oldScale;
			b.x = bx * SCALE + TILE_SIZE / 2 * SCALE;
			b.y = by * SCALE + TILE_SIZE / 2 * SCALE;
			b.width = b.width / oldScale * SCALE;
			b.height = b.height / oldScale * SCALE;
			b.updateBody();
		}
		
		for (Guard g : guards) {
			float gx = (g.x - TILE_SIZE / 2 * oldScale) / oldScale;
			float gy = (g.y - TILE_SIZE / 2 * oldScale) / oldScale;
			g.x = gx * SCALE + TILE_SIZE / 2 * SCALE;
			g.y = gy * SCALE + TILE_SIZE / 2 * SCALE;
			g.width = g.width / oldScale * SCALE;
			g.height = g.height / oldScale * SCALE;
			g.updateBody();
		}
	}

	@Override
	public void handleInput() {		
		camera.handleInput();
		mouse.x = Gdx.input.getX();
		mouse.y = Gdx.input.getY();
		camera.unproject(mouse);
		
		if (mouse.x > 0 && mouse.x < AccelShot.WIDTH && mouse.y > 0 && mouse.y < AccelShot.HEIGHT) {
			float rad = MathUtils.atan2(mouse.y - player.getY(), mouse.x - player.getX());
			player.setRadians(rad);
		}
		
		if (Gdx.input.justTouched() || Gdx.input.isKeyJustPressed(Keys.SPACE)) {
			player.addBullet();
		}
		
		if (Gdx.input.isKeyPressed(Keys.D)) {
			player.setDirection(Constants.DIRECTION_RIGHT, true);
		} else {
			player.setDirection(Constants.DIRECTION_RIGHT, false);
		}
		
		if (Gdx.input.isKeyPressed(Keys.W)) {
			player.setDirection(Constants.DIRECTION_UP, true);
		} else {
			player.setDirection(Constants.DIRECTION_UP, false);
		}
		
		if (Gdx.input.isKeyPressed(Keys.A)) {
			player.setDirection(Constants.DIRECTION_LEFT, true);
		} else {
			player.setDirection(Constants.DIRECTION_LEFT, false);
		}
		
		if (Gdx.input.isKeyPressed(Keys.S)) {
			player.setDirection(Constants.DIRECTION_DOWN, true);
		} else {
			player.setDirection(Constants.DIRECTION_DOWN, false);
		}
		
		//handle the bullet speed
		if (Gdx.input.isKeyJustPressed(Keys.NUM_1)) {
			player.toggleBulletSpeed(1);
		}
		if (Gdx.input.isKeyJustPressed(Keys.NUM_2)) {
			player.toggleBulletSpeed(2);
		}
		if (Gdx.input.isKeyJustPressed(Keys.NUM_3)) {
			player.toggleBulletSpeed(3);
		}
		if (Gdx.input.isKeyJustPressed(Keys.NUM_4)) {
			player.toggleBulletSpeed(4);
		}
		if (Gdx.input.isKeyJustPressed(Keys.NUM_5)) {
			player.toggleBulletSpeed(5);
		}
		if (Gdx.input.isKeyJustPressed(Keys.NUM_6)) {
			player.toggleBulletSpeed(6);
		}
		if (Gdx.input.isKeyJustPressed(Keys.NUM_7)) {
			player.toggleBulletSpeed(7);
		}
		if (Gdx.input.isKeyJustPressed(Keys.NUM_8)) {
			player.toggleBulletSpeed(8);
		}
		if (Gdx.input.isKeyJustPressed(Keys.NUM_9)) {
			player.toggleBulletSpeed(9);
		}
		if (Gdx.input.isKeyJustPressed(Keys.NUM_0)) {
			player.toggleBulletSpeed(0);
		}
		
		if (Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
			gsm.set(new TransitionState(gsm, this, new MenuState(gsm), new FadeTransition()));
		}
		
		if (Gdx.input.isKeyJustPressed(Keys.E)) {
			clock.stop();
			gsm.set(new VictoryState(gsm, this, clock, player.getBulletsUsed()));
			
		}
		if (Gdx.input.isKeyJustPressed(Keys.F)) {
			guardHit = true;
			shotTimer = shotTimerStart;
		}
		
		if (Gdx.input.isKeyJustPressed(Keys.ESCAPE) || Gdx.input.isKeyJustPressed(Keys.P)) {
			gsm.set(new PauseState(gsm, this));
		}
	}

	@Override
	public void update(float dt) {
		if (player != null) {
			player.update(dt);
			if (player.shouldRemove())
				player = null;
		}
		
		clock.update(dt);
		
		for (int i = guards.size - 1; i >= 0; i--) {
			guards.get(i).update(dt);
			guards.get(i).updateBody();
			if (guards.get(i).shouldRemove()) {
				guards.removeIndex(i);
			}
		}
		
		for (int i = bullets.size - 1; i >= 0; i--) {
			bullets.get(i).update(dt);
			if (bullets.get(i).shouldRemove()) {
				bullets.removeIndex(i);
			}
		}
		
		if (guardHit && shotTimer > 0) {
			shotTimer -= dt;
			if (shotTimer <= 0) {
				//guardHit = false;
				defeated = true;
				shotTimer = 0;
				clock.stop();
				player.setDirection(Constants.DIRECTION_DOWN, false);
				player.setDirection(Constants.DIRECTION_RIGHT, false);
				player.setDirection(Constants.DIRECTION_UP, false);
				player.setDirection(Constants.DIRECTION_LEFT, false);
				LoseCondition cond = new LoseCondition("Not fast enough!", LoseCondition.REASON_TOO_SLOW);
				gsm.set(new DefeatState(gsm, this, 1f, cond));
			}
		}
		
		if (!defeated)
			checkCollisions();
		
		if (!SANDBOX && guards.size == 0) {
			clock.stop();
			gsm.set(new VictoryState(gsm, this, clock, player.getBulletsUsed()));
		}
	}

	@Override
	public void render(SpriteBatch sb) {
		LevelReader.getTileMapRenderer().setView(camera);
		LevelReader.getTileMapRenderer().getSpriteBatch().begin();
		LevelReader.getTileMapRenderer().renderTileLayer(LevelReader.getBaseLayer());
		LevelReader.getTileMapRenderer().getSpriteBatch().end();
		
		if (AccelShot.DEBUG) {
			for (int i = 0; i < collisionBoxes.size; i++) {
				collisionBoxes.get(i).render(sb, sr);
			}
		}
		
		if (player != null)
			player.render(sb, sr);
		
		for (int i = 0; i < guards.size; i++) {
			guards.get(i).render(sb, sr);
		}
		for (int i = 0; i < bullets.size; i++) {
			bullets.get(i).render(sb, sr);
		}
		
		//render guard vision
		for (Guard g : guards) {
			g.renderVision();
		}

		if (guardHit) {
			sb.setProjectionMatrix(camera.combined);
			sb.begin();
			BitmapFont shottext = AccelShot.etext.getShotFont();
			float scale = defaultShotScaleX;
			if (shotTimer % 1f >= .75f) {
				float percentage = (shotTimer % 1f - .75f) / .25f;
				scale = percentage * 2 + 1f;
			}
			shottext.setScale(scale, scale);
			float num = (float) Calculations.round(shotTimer, 2, BigDecimal.ROUND_HALF_UP);
			float posx = AccelShot.WIDTH / 2 - shottext.getBounds("W.WW").width / 2;
			float posy = 40 + shottext.getBounds("W.WW").height / 2;
			String numrep = Float.toString(num);
			if (num == 0.00f)
				numrep = "0.00";
			if ((shotTimer / shotTimerStart <= .4f && shotTimer % 1f >= .5f) || shotTimer == 0.00f) {
				shottext.setColor(1, 0, 0, 1);
			} else {
				shottext.setColor(1, 1, 1, 1);
			}
			shottext.draw(sb, numrep, posx, posy);
			sb.end();
		}
		
		if (LevelReader.getTopLayer() != null) {
			LevelReader.getTileMapRenderer().setView(camera);
			LevelReader.getTileMapRenderer().getSpriteBatch().begin();
			LevelReader.getTileMapRenderer().renderTileLayer(LevelReader.getTopLayer());
			LevelReader.getTileMapRenderer().getSpriteBatch().end();
		}
		
		
		//draw clock
		if (!guardHit || shotTimer != 0)
			clock.render(sb);
	}
}
