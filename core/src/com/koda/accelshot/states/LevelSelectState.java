package com.koda.accelshot.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.utils.Array;
import com.koda.accelshot.AccelShot;
import com.koda.accelshot.handlers.GameData;
import com.koda.accelshot.handlers.LevelReader;
import com.koda.accelshot.handlers.Save;
import com.koda.accelshot.transitions.FadeTransition;
import com.koda.accelshot.ui.ImageButton;

public class LevelSelectState extends State {

	private final String title = "Select Level: ";
	private final String locked = "Locked";
	private final int LEFT = 0;
	private final int RIGHT = 1;
	private final int CAMERA_LEFT = 1;
	private final int CAMERA_RIGHT = -1;
	private final float baseCameraSpeed = 1;
	private final float maxCameraSpeed = 1500;
	private final float acceleration = 4500;
	private final float deceleration = 6000;
	private final float scale = AccelShot.SCALE * 3 / 4;
	private OrthographicCamera currentLevelSelectCamera;
	private OrthographicCamera oldLevelSelectCamera;
	private OrthogonalTiledMapRenderer currentRenderer;
	private OrthogonalTiledMapRenderer oldRenderer;
	private TiledMap currentMap;
	private TiledMap oldMap;
	private Array<ImageButton> buttons;
	private int level;
	private int oldLevel;
	private float offsetX;
	private float offsetY;
	private float lockedoffX;
	private float lockedoffY;
	private float cameraSpeed = baseCameraSpeed;
	private boolean scheduleUpdate;
	private boolean movingCameras;
	private boolean accelerating;
	private float cameraAccum;
	private float accumUntilMaxSpeed;
	private int cameraDirection;
	
	public LevelSelectState(GSM gsm, int level) {
		super(gsm);
		buttons = new Array<ImageButton>();
		Texture texleft = new Texture(Gdx.files.internal("leftarrow.png"));
		Texture texright = new Texture(Gdx.files.internal("rightarrow.png"));
		Sprite left = new Sprite(texleft);
		Sprite right = new Sprite(texright);
		left.setX(AccelShot.WIDTH / 16 - left.getWidth() / 2);
		left.setY(AccelShot.HEIGHT / 2 - left.getHeight() / 2);
		right.setX(AccelShot.WIDTH * 15 / 16 - right.getWidth() / 2);
		right.setY(AccelShot.HEIGHT / 2 - right.getHeight() / 2);
		buttons.add(new ImageButton(left, LEFT, 
				left.getX() + left.getWidth() / 2, 
				left.getY() + left.getHeight() / 2, 
				left.getWidth(), 
				left.getHeight()));
		buttons.add(new ImageButton(right, RIGHT, 
				right.getX() + right.getWidth() / 2, 
				right.getY() + right.getHeight() / 2, 
				right.getWidth(), 
				right.getHeight()));
		
		oldLevelSelectCamera = new OrthographicCamera();
		oldLevelSelectCamera.setToOrtho(false, AccelShot.WIDTH, AccelShot.HEIGHT);
		
		currentLevelSelectCamera = new OrthographicCamera();
		currentLevelSelectCamera.setToOrtho(false, AccelShot.WIDTH, AccelShot.HEIGHT);
		
		this.level = level;
		this.oldLevel = level;
		updateRenderer();
		updateButtons();
		
		offsetX = AccelShot.etext.getTitleFont().getBounds(title + "WW").width / 2;
		offsetY = AccelShot.etext.getTitleFont().getBounds(title + "WW").height / 2;
		lockedoffX = AccelShot.etext.getTitleFont().getBounds(locked).width / 2;
		lockedoffY = AccelShot.etext.getTitleFont().getBounds(locked).height / 2;		
	}
	
	public TiledMapTileLayer getLayer(TiledMap map, String layer) {
		return (TiledMapTileLayer) map.getLayers().get(layer);
	}
	
	private void updateRenderer() {
		currentLevelSelectCamera.setToOrtho(false, AccelShot.WIDTH, AccelShot.HEIGHT);
		if (level > oldLevel)
			currentLevelSelectCamera.translate(-3.25f * AccelShot.TILE_SIZE * AccelShot.SCALE - AccelShot.WIDTH, -AccelShot.TILE_SIZE * AccelShot.SCALE);
		else if (level < oldLevel)
			currentLevelSelectCamera.translate(-3.25f * AccelShot.TILE_SIZE * AccelShot.SCALE + AccelShot.WIDTH, -AccelShot.TILE_SIZE * AccelShot.SCALE);
		else
			currentLevelSelectCamera.translate(-3.25f * AccelShot.TILE_SIZE * AccelShot.SCALE, -AccelShot.TILE_SIZE * AccelShot.SCALE);
		currentLevelSelectCamera.update();

		oldLevelSelectCamera.setToOrtho(false, AccelShot.WIDTH, AccelShot.HEIGHT);
		oldLevelSelectCamera.translate(-3.25f * AccelShot.TILE_SIZE * AccelShot.SCALE, -AccelShot.TILE_SIZE * AccelShot.SCALE);
		oldLevelSelectCamera.update();
		
		try {
			currentMap = new TmxMapLoader().load("levels/accelshot_level" + level + ".tmx");
			oldMap = new TmxMapLoader().load("levels/accelshot_level" + oldLevel + ".tmx");
		} catch (Exception e) {
			e.printStackTrace();
			Gdx.app.exit();
		}
		
		currentRenderer = new OrthogonalTiledMapRenderer(currentMap, scale);
		oldRenderer = new OrthogonalTiledMapRenderer(oldMap, scale);
	}
	
	private void updateButtons() {
		for (ImageButton ib : buttons) {
			if (ib.getID() == LEFT) {
				if (level == 1) {
					ib.setCanBePressed(false);				
				} else {
					ib.setCanBePressed(true);
				}
			}
		
			if (ib.getID() == RIGHT) {
				if (level == GameData.LEVELS) {
					ib.setCanBePressed(false);
				} else {
					ib.setCanBePressed(true);
				}
			}
		}
	}
	
	private void decrementLevel() {
		oldLevel = level;
		level--;
		cameraDirection = CAMERA_RIGHT;
	}
	
	private void incrementLevel() {
		oldLevel = level;
		level++;
		cameraDirection = CAMERA_LEFT;
	}

	@Override
	public void handleInput() {
		if (Gdx.input.isTouched()) {
			mouse.x = Gdx.input.getX();
			mouse.y = Gdx.input.getY();
			camera.unproject(mouse);
			for (ImageButton ib : buttons) {
				if (!movingCameras && ib.canBePressed() && ib.contains(mouse.x, mouse.y)) {
					scheduleUpdate = true;
					if (ib.getID() == LEFT) {
						decrementLevel();
						ib.setPressed();
						
					} else if (ib.getID() == RIGHT) {
						incrementLevel();
						ib.setPressed();
					}
				}
			}
		} else {
			for (ImageButton ib : buttons) {
				ib.setUnpressed();
			}
		}
		
		if (Gdx.input.isKeyJustPressed(Keys.ENTER)) {
			if (!Save.gd.isLocked(level)) {
				gsm.set(new TransitionState(gsm, this, new PlayState(gsm, level), new FadeTransition()));
			}
		}
		
		if (!movingCameras && Gdx.input.isKeyJustPressed(Keys.LEFT)) {
			if (level > 1) {
				decrementLevel();
				scheduleUpdate = true;
			}
		}
		
		if (!movingCameras && Gdx.input.isKeyJustPressed(Keys.RIGHT)) {
			if (level < GameData.LEVELS) {
				incrementLevel();
				scheduleUpdate = true;
			}
		}
		
		if (scheduleUpdate) {
			updateButtons();
			updateRenderer();
			movingCameras = true;
			accelerating = true;
			scheduleUpdate = false;
			cameraSpeed = baseCameraSpeed;
			accumUntilMaxSpeed = 0;
		}
	}

	@Override
	public void update(float dt) {
		if (movingCameras) {
			if (cameraAccum < AccelShot.WIDTH) {
				float displacement = cameraDirection * dt * cameraSpeed;
				if (accelerating) {
					cameraSpeed += dt * acceleration;
					//System.out.println("Accelerating with speed: " + cameraSpeed);
				}
				else {
					if (cameraDirection == CAMERA_LEFT && currentLevelSelectCamera.position.x + accumUntilMaxSpeed >= AccelShot.WIDTH) {
						cameraSpeed -= dt * deceleration;
					} else if (cameraDirection == CAMERA_RIGHT 
							&& currentLevelSelectCamera.position.x - AccelShot.WIDTH - accumUntilMaxSpeed <= -AccelShot.WIDTH) {
						cameraSpeed -= dt * deceleration;
					}
				}
				if (cameraSpeed >= maxCameraSpeed) {
					accelerating = false;
				}
				cameraAccum += Math.abs(displacement);
				if (accelerating)
					accumUntilMaxSpeed += Math.abs(displacement);
				currentLevelSelectCamera.translate(displacement, 0);
				currentLevelSelectCamera.update();
				oldLevelSelectCamera.translate(displacement, 0);
				oldLevelSelectCamera.update();
				
				if (cameraAccum >= AccelShot.WIDTH) {
					currentLevelSelectCamera.position.set(
							-3.25f * AccelShot.TILE_SIZE * AccelShot.SCALE + AccelShot.WIDTH / 2, 
							-AccelShot.TILE_SIZE * AccelShot.SCALE + AccelShot.HEIGHT / 2, 0);
					currentLevelSelectCamera.update();
					movingCameras = false;
					cameraAccum = 0;
				}
			}
		}
	}

	@Override
	public void render(SpriteBatch sb) {
		currentRenderer.getSpriteBatch().setProjectionMatrix(currentLevelSelectCamera.combined);
		currentRenderer.setView(currentLevelSelectCamera);
		currentRenderer.getSpriteBatch().begin();
		if (Save.gd.isLocked(level)) {
			currentRenderer.getSpriteBatch().setColor(1, 1, 1, .25f);
		} else {
			currentRenderer.getSpriteBatch().setColor(1, 1, 1, 1);
		}
		currentRenderer.renderTileLayer(getLayer(currentMap, "base"));
		currentRenderer.renderTileLayer(getLayer(currentMap, "top"));
		currentRenderer.getSpriteBatch().end();
		
		if (movingCameras) {
			oldRenderer.getSpriteBatch().setProjectionMatrix(oldLevelSelectCamera.combined);
			oldRenderer.setView(oldLevelSelectCamera);
			oldRenderer.getSpriteBatch().begin();
			if (Save.gd.isLocked(oldLevel)) {
				oldRenderer.getSpriteBatch().setColor(1, 1, 1, .25f);
			} else {
				oldRenderer.getSpriteBatch().setColor(1, 1, 1, 1);
			}
			oldRenderer.renderTileLayer(getLayer(oldMap, "base"));
			oldRenderer.renderTileLayer(getLayer(oldMap, "top"));
			oldRenderer.getSpriteBatch().end();
		}
		
		sb.setProjectionMatrix(camera.combined);
		sb.begin();
		for (ImageButton ib : buttons) {
			if (ib.canBePressed()) {
				ib.getImage().setAlpha(1);
			} else {
				ib.getImage().setAlpha(.125f);
			}
			
			ib.getImage().draw(sb);
		}
		
		String combinedTitle = title + level;
		AccelShot.etext.getTitleFont().setColor(1, 1, 1, 1);
		AccelShot.etext.getTitleFont().draw(sb, combinedTitle, AccelShot.WIDTH / 2 - offsetX, AccelShot.HEIGHT * 9 / 10 + offsetY);
		if (!movingCameras && Save.gd.isLocked(level)) {
			AccelShot.etext.getTitleFont().setColor(1, 0, 0, 1);
			AccelShot.etext.getTitleFont().draw(sb, locked, AccelShot.WIDTH / 2 - lockedoffX, AccelShot.HEIGHT / 2 + lockedoffY);
		}
		sb.end();
	}
}
