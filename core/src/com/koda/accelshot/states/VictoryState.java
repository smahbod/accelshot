package com.koda.accelshot.states;

import static com.koda.accelshot.handlers.Constants.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.koda.accelshot.AccelShot;
import com.koda.accelshot.handlers.Constants;
import com.koda.accelshot.handlers.GameData;
import com.koda.accelshot.handlers.LevelReader;
import com.koda.accelshot.transitions.FadeTransition;
import com.koda.accelshot.ui.Clock;
import com.koda.accelshot.ui.Score;
import com.koda.accelshot.ui.TextButton;

public class VictoryState extends State {

	private final String NEXT_LEVEL = "Next Level";
	private final String LEVEL_SELECT = "Back to Level Select";
	private final String RETRY = "Retry";
	private final String MAIN_MENU = "Main Menu";
	private Texture texture;
	private Sprite sprite;
	private State display;
	private Clock clock;
	private Score score;
	private Array<TextButton> buttons;
	private float timer;
	private final float maxTime = 2f;
	private int bulletsUsed;
	private int totalTimeScore;
	private int totalBulletScore;
	
	public VictoryState(GSM gsm, State display, Clock clock, int bulletsUsed) {
		super(gsm);
		this.display = display;
		this.clock = clock;
		this.bulletsUsed = bulletsUsed;
		score = new Score(0, -(AccelShot.WIDTH / 2), AccelShot.HEIGHT / 2 - 50, AccelShot.WIDTH / 4, AccelShot.HEIGHT / 2 - 50);
		texture = new Texture(Gdx.files.internal("rx face.jpg"));
		sprite = new Sprite(texture);
		clock.setTarget(AccelShot.WIDTH / 4, AccelShot.HEIGHT / 2 - 200);
		score.setMaxTime(maxTime);
		clock.setMaxTime(maxTime);
		
		if (clock.getTime() <= Constants.TIME_SCORE_THRESHOLD) {
			totalTimeScore = Constants.TIME_SCORE_MULTIPLIER * (Constants.TIME_SCORE_THRESHOLD - clock.getTime());
			score.setTimeBonus(totalTimeScore);
			score.addScore(totalTimeScore);
		}
		
		if (bulletsUsed <= Constants.BULLET_SCORE_THRESHOLD) {
			totalBulletScore = (Constants.BULLET_SCORE_THRESHOLD - bulletsUsed + 1) * Constants.BULLET_SCORE_MULTIPLIER;
			score.setBulletBonus(totalBulletScore);
			score.addScore(totalBulletScore);
		}
		
		score.checkHighScore();
		
		buttons = new Array<TextButton>();
	}
	
	@Override
	protected void drawUI() {
		
		BitmapFont font = AccelShot.etext.getMenuFont();
		if (LevelReader.getLevel() < GameData.LEVELS) {
			buttons.add(new TextButton(NEXT_LEVEL, BUTTON_NEXT_LEVEL,
					AccelShot.WIDTH * 3 / 4, 
					AccelShot.HEIGHT / 2, 
					font.getBounds(NEXT_LEVEL).width * 1.5f, 
					font.getBounds(NEXT_LEVEL).height * 2f));
		}
		
		buttons.add(new TextButton(RETRY, BUTTON_RETRY, 
				AccelShot.WIDTH * 3 / 4, 
				AccelShot.HEIGHT / 2 - 1 * AccelShot.TILE_SIZE * AccelShot.SCALE, 
				font.getBounds(RETRY).width * 1.5f, 
				font.getBounds(RETRY).height * 2f));
		
		buttons.add(new TextButton(LEVEL_SELECT, BUTTON_LEVEL_SELECT, 
				AccelShot.WIDTH * 3 / 4, 
				AccelShot.HEIGHT / 2 - 2 * AccelShot.TILE_SIZE * AccelShot.SCALE, 
				font.getBounds(LEVEL_SELECT).width * 1.5f, 
				font.getBounds(LEVEL_SELECT).height * 2f));
		
		buttons.add(new TextButton(MAIN_MENU, BUTTON_MAIN_MENU, 
				AccelShot.WIDTH * 3 / 4, 
				AccelShot.HEIGHT / 2 - 3 * AccelShot.TILE_SIZE * AccelShot.SCALE, 
				font.getBounds(MAIN_MENU).width * 1.5f, 
				font.getBounds(MAIN_MENU).height * 2f));
	}

	@Override
	public void handleInput() {
		mouse.x = Gdx.input.getX();
		mouse.y = Gdx.input.getY();
		camera.unproject(mouse);
		for (TextButton tb : buttons) {
			if (tb.contains(mouse.x, mouse.y)) {
				tb.color.set(1, 0, 0, 1);
			} else {
				tb.color.set(1, 1, 1, 1);
			}
		}
		
		if (Gdx.input.justTouched()) {
			for (TextButton tb : buttons) {
				if (tb.contains(mouse.x, mouse.y)) {
					switch (tb.getID()) {
					case BUTTON_NEXT_LEVEL:
						gsm.set(new TransitionState(gsm, this, new PlayState(gsm, LevelReader.getLevel() + 1), new FadeTransition()));
						break;
					case BUTTON_LEVEL_SELECT:
						int level = LevelReader.getLevel() < GameData.LEVELS ? LevelReader.getLevel() + 1 : LevelReader.getLevel();
						gsm.set(new TransitionState(gsm, this, new LevelSelectState(gsm, level), new FadeTransition()));
						break;
					case BUTTON_RETRY:
						gsm.set(new TransitionState(gsm, this, new PlayState(gsm, LevelReader.getLevel()), new FadeTransition()));
						break;
					case BUTTON_MAIN_MENU:
						gsm.set(new TransitionState(gsm, this, new MenuState(gsm), new FadeTransition()));
						break;
					}
				}
			}		
		}
		
		if (timer == maxTime && Gdx.input.isKeyJustPressed(Keys.ANY_KEY)) {
			//gsm.set(new TransitionState(gsm, this, new MenuState(gsm), new FadeTransition()));
			score.skip();
		}
	}

	@Override
	public void update(float dt) {
		if (timer < maxTime) {
			timer += dt;
			if (timer >= maxTime) {
				timer = maxTime;
				drawUI();
			}
		}
		
		clock.update(dt);
		score.update(dt);
	}

	@Override
	public void render(SpriteBatch sb) {
		if (timer < maxTime) {
			display.render(sb);
		} else {
			sb.begin();
			for (TextButton tb : buttons) {
				AccelShot.etext.getMenuFont().setColor(tb.color);
				AccelShot.etext.getMenuFont().draw(sb, tb.getText(),
						tb.x - AccelShot.etext.getMenuFont().getBounds(tb.getText()).width / 2, 
						tb.y + AccelShot.etext.getMenuFont().getBounds(tb.getText()).height / 2);
			}
			sb.end();
		}
		
		//sb.setProjectionMatrix(camera.combined);
		sb.begin();
		float x = AccelShot.WIDTH * timer / maxTime - AccelShot.WIDTH;
		
		sprite.setColor(1, 1, 1, .3f);
		sprite.setX(x);
		sprite.setY(0);
		sprite.setSize(AccelShot.WIDTH, AccelShot.HEIGHT);
		sprite.draw(sb);
		String gameover = "Level clear";
		BitmapFont postgame = AccelShot.etext.getPostGameFont();
		postgame.setColor(1, 0, 0, 1);
		float pos = x + AccelShot.WIDTH / 2 - postgame.getBounds(gameover).width / 2;
		postgame.draw(sb, gameover, pos, AccelShot.HEIGHT * 2 / 3);
		sb.end();
		
		clock.render(sb);
		score.render(sb);
	}
}
