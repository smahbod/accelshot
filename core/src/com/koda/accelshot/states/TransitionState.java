package com.koda.accelshot.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.koda.accelshot.AccelShot;
import com.koda.accelshot.transitions.FadeTransition;
import com.koda.accelshot.transitions.Transition;

public class TransitionState extends State {

	private State previous;
	private State next;
	private Transition transition;
	
	public TransitionState(GSM gsm, State previous, State next, Transition transition) {
		super(gsm);
		this.previous = previous;
		this.next = next;
		this.transition = transition;
	}

	@Override
	public void handleInput() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(float dt) {
		transition.update(dt);
		if (transition.phaseSwitched()) {
			next.init();
		}
		if (transition.getPhase() == 2) {
			gsm.set(next);
		}
	}

	@Override
	public void render(SpriteBatch sb) {
		if (transition.getPhase() == 0) {
			previous.render(sb);
		} else if (transition.getPhase() == 1) {
			next.render(sb);
		}
		transition.render(sb);
	}
}
