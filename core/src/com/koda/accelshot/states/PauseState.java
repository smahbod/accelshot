package com.koda.accelshot.states;

import static com.koda.accelshot.handlers.Constants.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.koda.accelshot.AccelShot;
import com.koda.accelshot.handlers.LevelReader;
import com.koda.accelshot.transitions.FadeTransition;
import com.koda.accelshot.ui.TextButton;

public class PauseState extends State {

	private final String RESUME = "Resume";
	private final String RESTART = "Restart";
	private final String MAIN_MENU = "Main Menu";
	private BitmapFont font;
	private State toReturn;
	private Array<TextButton> buttons;
	private float timer;
	private float maxTime = 1f;
	private final int GOING_WHITE = 0;
	private final int GOING_GRAY = 1;
	private int phase = GOING_GRAY;
	
	public PauseState(GSM gsm, State toReturn) {
		super(gsm);
		this.toReturn = toReturn;
		this.runningState = toReturn;
		font = AccelShot.etext.getMenuFont();
		
		buttons = new Array<TextButton>();
		buttons.add(new TextButton(RESUME, BUTTON_RESUME,
				AccelShot.WIDTH / 2, 
				AccelShot.HEIGHT / 2 - 1.5f * AccelShot.TILE_SIZE * AccelShot.SCALE, 
				font.getBounds(RESUME).width * 1.5f, 
				font.getBounds(RESUME).height * 2f));
		
		buttons.add(new TextButton(RESTART, BUTTON_RETRY, 
				AccelShot.WIDTH / 2, 
				AccelShot.HEIGHT / 2 - 2.5f * AccelShot.TILE_SIZE * AccelShot.SCALE, 
				font.getBounds(RESTART).width * 1.5f, 
				font.getBounds(RESTART).height * 2f));
		
		buttons.add(new TextButton(MAIN_MENU, BUTTON_MAIN_MENU, 
				AccelShot.WIDTH / 2, 
				AccelShot.HEIGHT / 2 - 3.5f * AccelShot.TILE_SIZE * AccelShot.SCALE, 
				font.getBounds(MAIN_MENU).width * 1.5f, 
				font.getBounds(MAIN_MENU).height * 2f));
	}
	
	@Override
	protected void drawUI() {
		
	}

	@Override
	public void handleInput() {
		mouse.x = Gdx.input.getX();
		mouse.y = Gdx.input.getY();
		camera.unproject(mouse);
		for (TextButton tb : buttons) {
			if (tb.contains(mouse.x, mouse.y)) {
				tb.color.set(1, 0, 0, 1);
			} else {
				tb.color.set(1, 1, 1, 1);
			}
		}
		
		if (Gdx.input.justTouched()) {
			for (TextButton tb : buttons) {
				if (tb.contains(mouse.x, mouse.y)) {
					switch (tb.getID()) {
					case BUTTON_RESUME:
						gsm.set(toReturn);
						break;
					case BUTTON_RETRY:
						gsm.set(new TransitionState(gsm, this, new PlayState(gsm, LevelReader.getLevel()), new FadeTransition()));
						break;
					case BUTTON_MAIN_MENU:
						gsm.set(new TransitionState(gsm, this, new MenuState(gsm), new FadeTransition()));
						break;
					}
				}
			}		
		}
		
		if (Gdx.input.isKeyJustPressed(Keys.P) || Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
			gsm.set(toReturn);
		}
	}

	@Override
	public void update(float dt) {
		if (timer < maxTime) {
			timer += dt;
			if (timer >= maxTime) {
				timer = 0;
				phase = (phase + 1) % 2;
			}
		}
	}

	@Override
	public void render(SpriteBatch sb) {
		toReturn.render(sb);
		
		sb.setProjectionMatrix(camera.combined);
		sb.begin();
		sb.setColor(1, 1, 1, .5f);
		sb.draw(AccelShot.res.getAtlas("pack").findRegion("black"), 0, 0, AccelShot.WIDTH, AccelShot.HEIGHT);
		String gameover = "Pause";
		BitmapFont postgame = AccelShot.etext.getPostGameFont();
		float scale = 0;
		if (phase == GOING_WHITE)
			scale = timer / maxTime * .5f + .5f;
		else
			scale = 1 - (timer / maxTime * .5f);
		postgame.setColor(scale, scale, scale, 1);
		float pos = AccelShot.WIDTH / 2 - postgame.getBounds(gameover).width / 2;
		postgame.draw(sb, gameover, pos, AccelShot.HEIGHT / 2);
		sb.end();
		
		sb.begin();
		for (TextButton tb : buttons) {
			font.setColor(tb.color);
			font.draw(sb, tb.getText(),
					tb.x - font.getBounds(tb.getText()).width / 2, 
					tb.y + font.getBounds(tb.getText()).height / 2);
		}
		sb.end();
	}
}
