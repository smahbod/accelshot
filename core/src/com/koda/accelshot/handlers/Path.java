package com.koda.accelshot.handlers;

import static com.koda.accelshot.entities.Entity.pi;
import static com.koda.accelshot.entities.Guard.COUNTER_CLOCKWISE;

import com.badlogic.gdx.math.MathUtils;

public class Path {

	private static final int FIELDS = 4; //change this to the number of public fields below, except rw
	public float x;
	public float y;
	public float speed;
	public float delay;
	public RotationWaypoint[] rw;
	
	public Path(float x, float y, float speed, float delay, RotationWaypoint[] rw) {
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.delay = delay;
		this.rw = rw;
	}
	
	public static Path[] generatePath(float[] path, RotationWaypoint[][] rw) {
		Path[] p = new Path[path.length / FIELDS];
		for (int i = 0; i < p.length; i++) {
			p[i] = new Path(path[i * FIELDS], path[i * FIELDS + 1], path[i * FIELDS + 2], path[i * FIELDS + 3], rw[i]);
		}
		return p;
	}
	
	public static Path[] generatePath(float[] path) {
		Path[] p = new Path[path.length / FIELDS];
		for (int i = 0; i < p.length; i++) {
			p[i] = new Path(path[i * FIELDS], path[i * FIELDS + 1], path[i * FIELDS + 2], path[i * FIELDS + 3], null);
		}
		return p;
	}
	
	public static Path[] generateSamplePath(float x, float y) {
		float[] path = new float[] {
				x, y, 100, 1,
				x, y - 100, 100, 1,
				x - 100, y - 100, 100, 1,
				x - 100, y, 100, 1
		};
		
		float[] rp1 = new float[] {
				pi/2, COUNTER_CLOCKWISE, 2f, 1f,
				3*pi/4, COUNTER_CLOCKWISE, 2.5f, 1f
		};
		
		float[] rp2 = new float[] {
				pi/2, COUNTER_CLOCKWISE, 2f, 1f,
				3*pi/4, COUNTER_CLOCKWISE, 2.5f, 1f
		};
		
		float[] rp3 = new float[] {
				pi/2, COUNTER_CLOCKWISE, 2f, 1f,
				3*pi/4, COUNTER_CLOCKWISE, 2.5f, 1f
		};
		
		float[] rp4 = new float[] {
				pi/2, COUNTER_CLOCKWISE, 2f, 1f,
				3*pi/4, COUNTER_CLOCKWISE, 2.5f, 1f
		};
		
		RotationWaypoint[] rw1 = RotationWaypoint.generateRotationWaypoint(rp1);
		RotationWaypoint[] rw2 = RotationWaypoint.generateRotationWaypoint(rp2);
		RotationWaypoint[] rw3 = RotationWaypoint.generateRotationWaypoint(rp3);
		RotationWaypoint[] rw4 = RotationWaypoint.generateRotationWaypoint(rp4);
		return generatePath(path, new RotationWaypoint[][] {rw1, rw2, rw3, rw4});
	}
	
	public static float radiansToNext(Path[] p, int index) {
		int next = (index + 1) % p.length;
		return MathUtils.atan2(p[next].y - p[index].y, p[next].x - p[index].x);
	}
	
	@Override
	public String toString() {
		String s = "";
		if (rw == null)
			s = "none";
		else {
			for (int i = 0; i < rw.length; i++) {
				s += rw[i];
				if (i != rw.length - 1)
					s += "\n";
			}
		}
		
		return "x = " + x + ", y = " + y + ", speed = " + speed + ", delay = " + delay + ", rotation waypoint = " + s;
	}
}
