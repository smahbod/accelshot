package com.koda.accelshot.handlers;

public class Constants {

	//directionals
	public static final int DIRECTION_RIGHT = 0;
	public static final int DIRECTION_UP = 1;
	public static final int DIRECTION_LEFT = 2;
	public static final int DIRECTION_DOWN = 3;
	
	//animation directions
	public static final int ANIMATION_DOWN = 0;
	public static final int ANIMATION_DOWN_LEFT = 1;
	public static final int ANIMATION_DOWN_RIGHT = 2;
	public static final int ANIMATION_UP = 3;
	public static final int ANIMATION_UP_LEFT = 4;
	public static final int ANIMATION_UP_RIGHT = 5;
	public static final int ANIMATION_LEFT = 6;
	public static final int ANIMATION_LEFT_UP = 7;
	public static final int ANIMATION_LEFT_DOWN = 8;
	public static final int ANIMATION_RIGHT = 9;
	public static final int ANIMATION_RIGHT_UP = 10;
	public static final int ANIMATION_RIGHT_DOWN = 11;
	
	//robot animations
	public static final int ANIMATION_ROBOT_DOWN = 0;
	public static final int ANIMATION_ROBOT_UP = 1;
	public static final int ANIMATION_ROBOT_RIGHT = 2;
	public static final int ANIMATION_ROBOT_LEFT = 3;
	public static final int ANIMATION_ROBOT_DIAGONALS = 4;
	public static final int ANIMATION_ROBOT_DOWN_LEFT = 0;
	public static final int ANIMATION_ROBOT_DOWN_RIGHT = 1;
	public static final int ANIMATION_ROBOT_UP_LEFT = 2;
	public static final int ANIMATION_ROBOT_UP_RIGHT = 3;
		
	//score
	public static final int TIME_SCORE_THRESHOLD = 180;
	public static final int TIME_SCORE_MULTIPLIER = 5;
	public static final int BULLET_SCORE_THRESHOLD = 10;
	public static final int BULLET_SCORE_MULTIPLIER = 15;

	//button names
	/*public static final String BUTTON_PLAY = "play";
	public static final String BUTTON_SCORES = "scores";
	public static final String BUTTON_EXIT = "quit";
	public static final String BUTTON_RESUME = "resume";
	public static final String BUTTON_NEXT_LEVEL = "nextlevel";
	public static final String BUTTON_RETRY = "retry";
	public static final String BUTTON_MAIN_MENU = "mainmenu";*/
	
	public static final int BUTTON_LEVEL_SELECT = 0;
	public static final int BUTTON_SCORES = 1;
	public static final int BUTTON_EXIT = 2;
	public static final int BUTTON_RESUME = 3;
	public static final int BUTTON_NEXT_LEVEL = 4;
	public static final int BUTTON_RETRY = 5;
	public static final int BUTTON_MAIN_MENU = 6;
}
