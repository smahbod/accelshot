package com.koda.accelshot.handlers;

import java.io.Serializable;

public class GameData implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final int LEVELS = 5;
	
	private int[] times;
	private int[] bulletsFired;
	private int[] scores;
	private boolean[] locked;
	
	public GameData() {
		times = new int[LEVELS];
		bulletsFired = new int[LEVELS];
		scores = new int[LEVELS];
		locked = new boolean[LEVELS];
	}
	
	public int[] getTimes() {
		return times;
	}
	
	public int[] getBulletsFired() {
		return bulletsFired;
	}
	
	public int[] getScores() {
		return scores;
	}
	
	public boolean isLocked(int level) {
		return locked[level - 1];
	}
	
	public void init() {
		for (int i = 0; i < LEVELS; i++) {
			times[i] = -1;
			bulletsFired[i] = -1;
			scores[i] = -1;
			if (i != 0)
				locked[i] = true;
		}
	}
	
	public boolean addEntry(int level, int time, int bullets, int score) {
		int i = level - 1;
		if (i < locked.length - 1)
			locked[i + 1] = false;
		if (times[i] == -1 || score > scores[i]) {
			times[i] = time;
			bulletsFired[i] = bullets;
			scores[i] = score;
			return true;
		}
		return false;
	}
}
