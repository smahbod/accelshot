package com.koda.accelshot.handlers;

import static com.koda.accelshot.entities.Entity.pi;
import static com.koda.accelshot.entities.Guard.COUNTER_CLOCKWISE;
import static com.koda.accelshot.entities.Guard.CLOCKWISE;

public class RotationWaypoint {

	private static final int FIELDS = 4; //change this to the number of public fields below
	public float distance;
	public float delay;
	public float turnRate;
	public int direction;
	
	public RotationWaypoint(float distance, int direction, float turnRate, float delay) {
		this.distance = distance;
		this.turnRate = turnRate;
		this.direction = direction;
		this.delay = delay;
	}
	
	public static RotationWaypoint[] generateRotationWaypoint(float[] waypoints) {
		RotationWaypoint[] rotationWaypoints = new RotationWaypoint[waypoints.length / FIELDS];
		for (int i = 0; i < rotationWaypoints.length; i++) {
			rotationWaypoints[i] = new RotationWaypoint(waypoints[i * FIELDS], (int) waypoints[i * FIELDS + 1], 
					waypoints[i * FIELDS + 2], waypoints[i * FIELDS + 3]);
		}
		return rotationWaypoints;
	}
	
	public static RotationWaypoint[] generateSampleRotationWaypint() {
		float[] rp = {
				2*pi/3, COUNTER_CLOCKWISE, 2f, 1,
				2*pi/3, COUNTER_CLOCKWISE, 3f, 1,
				4*pi/3, CLOCKWISE, 4f, 1
		};
		
		return generateRotationWaypoint(rp);
	}
	
	public static RotationWaypoint[] generateSampleRotationWaypint2() {
		float[] rp = {
				pi/2, COUNTER_CLOCKWISE, 2f, 1,
				pi/2, COUNTER_CLOCKWISE, 2f, 1,
				pi/2, COUNTER_CLOCKWISE, 2f, 1,
				pi/2, COUNTER_CLOCKWISE, 2f, 1
		};
		
		return generateRotationWaypoint(rp);
	}
	
	@Override
	public String toString() {
		return "distance = " + distance + ", turn rate = " + turnRate + ", direction = " + direction + ", delay = " + delay;
	}
}
