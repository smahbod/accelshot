package com.koda.accelshot.handlers;

import com.badlogic.gdx.graphics.OrthographicCamera;

public class MovingCamera extends OrthographicCamera {

	private int offset;
	private float xmin;
	private float ymin;
	private float xmax;
	private float ymax;
	
	public MovingCamera() {
		super();
	}
	
	public void setBounds(float xmin, float xmax, float ymin, float ymax) {
		this.xmin = xmin;
		this.xmax = xmax;
		this.ymin = ymin;
		this.ymax = ymax;
	}
	
	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	public void setPosition(float x, float y) {
		position.set(x, y, 0);
		fixBounds();
	}
	
	private void fixBounds() {
		if(position.x < xmin - offset + viewportWidth / 2) {
			position.x = xmin - offset + viewportWidth / 2;
		}
		if(position.x > xmax + offset - viewportWidth / 2) {
			position.x = xmax + offset - viewportWidth / 2;
		}
		if(position.y < ymin - offset + viewportHeight / 2) {
			position.y = ymin - offset + viewportHeight / 2;
		}
		if(position.y > ymax- viewportHeight / 2) {
			position.y = ymax- viewportHeight / 2;
		}
	}
	
	public void handleInput() {
		/*if (Gdx.input.isKeyPressed(Keys.EQUALS)) {
			System.out.println("zoomed out, z = ");
			zoom -= 0.06;
		}
		
		if (Gdx.input.isKeyPressed(Keys.MINUS)) {
			zoom += 0.06;
		}
		update();*/
	}
}
