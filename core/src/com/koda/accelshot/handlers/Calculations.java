package com.koda.accelshot.handlers;

import java.math.BigDecimal;

public class Calculations {

	public static double round(double unrounded, int precision, int roundingMode)
	{
	    BigDecimal bd = new BigDecimal(unrounded);
	    BigDecimal rounded = bd.setScale(precision, roundingMode);
	    return rounded.doubleValue();
	}
	
	public static float distanceBetween(float x1, float y1, float x2, float y2) {
		return (float) Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
	}
	
	public static String getDigits(float b) {
		int num = (int) b;
		String result = "W";
		while (num / 10 != 0) {
			result += "W";
			num /= 10;
		}
		return result;
	}
}
