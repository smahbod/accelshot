package com.koda.accelshot.handlers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class EntityText {

	private BitmapFont fps;
	private BitmapFont indicator;
	private BitmapFont title;
	private BitmapFont menuOptions;
	private BitmapFont shotIndicator;
	private BitmapFont postGame;
	private BitmapFont time;
	private BitmapFont score;
	
	public EntityText() {
		FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("Hyperspace Bold.ttf"));
		
		FreeTypeFontParameter ftfp = new FreeTypeFontParameter();
		ftfp.size = 16;
		indicator = gen.generateFont(ftfp);
		fps = gen.generateFont(ftfp);
		ftfp.size = 48;
		title = gen.generateFont(ftfp);
		ftfp.size = 24;
		menuOptions = gen.generateFont(ftfp);
		time = gen.generateFont(ftfp);
		ftfp.size = 30;
		shotIndicator = gen.generateFont(ftfp);
		ftfp.size = 30;
		postGame = gen.generateFont(ftfp);
		score = gen.generateFont(ftfp);
		
	}
	
	public BitmapFont getFpsFont() {
		return fps;
	}
	
	public BitmapFont getFont() {
		return indicator;
	}

	public BitmapFont getTitleFont() {
		return title;
	}
	
	public BitmapFont getMenuFont() {
		return menuOptions;
	}
	
	public BitmapFont getShotFont() {
		return shotIndicator;
	}
	
	public BitmapFont getPostGameFont() {
		return postGame;
	}
	
	public BitmapFont getTimeFont() {
		return time;
	}
	
	public BitmapFont getScoreFont() {
		return score;
	}
}
