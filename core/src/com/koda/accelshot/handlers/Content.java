package com.koda.accelshot.handlers;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class Content {

	private HashMap<String, TextureAtlas> atlas;
	
	public Content() {
		atlas = new HashMap<String, TextureAtlas>();
	}
	
	public void loadAtlas(String key, String path) {
		atlas.put(key, new TextureAtlas(Gdx.files.internal(path)));
	}
	
	public TextureAtlas getAtlas(String key) {
		return atlas.get(key);
	}
}
