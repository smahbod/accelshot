package com.koda.accelshot.handlers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.badlogic.gdx.Gdx;

public class Save {
	
	public static final String filename = "scores.dat";
	public static GameData gd;
	
	public static void save() {
		try {
			ObjectOutputStream out = new ObjectOutputStream(
				new FileOutputStream(filename)
			);
			out.writeObject(gd);
			out.close();
		}
		catch(Exception e) {
			e.printStackTrace();
			Gdx.app.exit();
		}
	}
	
	public static void load() {
		try {
			if(!saveFileExists()) {
				init();
				return;
			}
			ObjectInputStream in = new ObjectInputStream(
				new FileInputStream(filename)
			);
			gd = (GameData) in.readObject();
			in.close();
		}
		catch(Exception e) {
			e.printStackTrace();
			Gdx.app.exit();
		}
	}
	
	public static boolean saveFileExists() {
		File f = new File(filename);
		return f.exists();
	}
	
	public static void init() {
		System.out.println("Save file doesn't exist, creating it");
		gd = new GameData();
		gd.init();
		save();
	}
}
