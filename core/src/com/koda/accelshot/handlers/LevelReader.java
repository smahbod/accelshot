package com.koda.accelshot.handlers;

import static com.koda.accelshot.AccelShot.SCALE;
import static com.koda.accelshot.AccelShot.TILE_SIZE;
import static com.koda.accelshot.entities.Entity.pi;
import static com.koda.accelshot.entities.Entity.STD_WIDTH;
import static com.koda.accelshot.entities.Entity.STD_HEIGHT;
import static com.koda.accelshot.entities.Guard.*;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.utils.Array;
import com.koda.accelshot.entities.CollisionBox;
import com.koda.accelshot.entities.Guard;
import com.koda.accelshot.entities.MultipleViewGuard;
import com.koda.accelshot.entities.PathGuard;
import com.koda.accelshot.entities.Player;
import com.koda.accelshot.entities.RotatingGuard;
import com.koda.accelshot.entities.SimpleGuard;

public class LevelReader {

	private static OrthogonalTiledMapRenderer tmrenderer;
	private static TiledMap tileMap;
	private static Array<CollisionBox> collisionBoxes;
	private static Array<Guard> guards;
	private static Player player;
	private static int level;
	
	public static OrthogonalTiledMapRenderer getTileMapRenderer() {
		return tmrenderer;
	}
	
	public static TiledMapTileLayer getBaseLayer() {
		return (TiledMapTileLayer) tileMap.getLayers().get("base");
	}
	
	public static TiledMapTileLayer getTopLayer() {
		return (TiledMapTileLayer) tileMap.getLayers().get("top");
	}
	
	public static Array<CollisionBox> getCollisionBoxes() {
		return collisionBoxes;
	}
	
	public static Array<Guard> getGuards() {
		return guards;
	}
	
	public static Player getPlayer() {
		return player;
	}
	
	public static int getLevel() {
		return level;
	}
	
	public static void initializeRenderer(int level, float scale) {
		try {
			tileMap = new TmxMapLoader().load("levels/accelshot_level" + level + ".tmx");
			LevelReader.level = level;
		} catch (Exception e) {
			e.printStackTrace();
			Gdx.app.exit();
		}
		tmrenderer = new OrthogonalTiledMapRenderer(tileMap, scale);
	}
	
	public static void load(int level) {
		try {
			tileMap = new TmxMapLoader().load("levels/accelshot_level" + level + ".tmx");
		} catch (Exception e) {
			e.printStackTrace();
			Gdx.app.exit();
		}
		
		collisionBoxes = new Array<CollisionBox>();
		guards = new Array<Guard>();
		
		TiledMapTileLayer layer = (TiledMapTileLayer) tileMap.getLayers().get("collisions");
		int index = 1;
		for (int row = 0; row < layer.getHeight(); row++) {
			for (int col = 0; col < layer.getWidth(); col++) {
				if (layer.getCell(col, row) != null) {
					float adjustedX = col * TILE_SIZE / 2 * SCALE + TILE_SIZE / 2 * SCALE;
					float adjustedY = row * TILE_SIZE / 2 * SCALE + TILE_SIZE / 2 * SCALE;
					float adjustedWidth = TILE_SIZE * SCALE;
					float adjustedHeight = TILE_SIZE * SCALE;
					collisionBoxes.add(new CollisionBox(adjustedX, adjustedY, adjustedWidth, adjustedHeight, index++));
				}
			}
		}
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse("../core/assets/leveldata/accelshot_level" + level + ".xml");
			document.normalize();
			
			Node root = document.getElementsByTagName("leveldata").item(0);
			Element rootElement = (Element) root;
			Node player = rootElement.getElementsByTagName("player").item(0);
			Element playerElement = (Element) player;
			createPlayer(playerElement);
			NodeList guardList = rootElement.getElementsByTagName("guard");
			for (int i = 0; i < guardList.getLength(); i++) {
				Node guard = guardList.item(i);
				Element guardElement = (Element) guard;
				createGuard(guardElement);
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void createPlayer(Element elmt) {
		float x = Float.parseFloat(elmt.getAttribute("x")) * TILE_SIZE * SCALE + TILE_SIZE / 2 * SCALE;
		float y = Float.parseFloat(elmt.getAttribute("y")) * TILE_SIZE * SCALE + TILE_SIZE / 2 * SCALE;
		float width = TILE_SIZE * SCALE * 3 / 5;
		float height = TILE_SIZE * SCALE * 4 / 5;
		player = new Player(x, y, width, height);
	}

	private static void createGuard(Element elmt) {
		float x = Float.parseFloat(elmt.getAttribute("x")) * TILE_SIZE * SCALE + TILE_SIZE / 2 * SCALE;
		float y = Float.parseFloat(elmt.getAttribute("y")) * TILE_SIZE * SCALE + TILE_SIZE / 2 * SCALE;
		float width = STD_WIDTH;
		float height = STD_HEIGHT;
		float direction = Float.parseFloat(elmt.getAttribute("direction")) * pi / 180;
		
		String type = elmt.getAttribute("type");
		if (type.equals("simple")) {
			//guards.add(new SimpleGuard(x, y, STD_WIDTH, STD_HEIGHT, direction));
		} else if (type.equals("multiview")) {
			int views = Integer.parseInt(elmt.getAttribute("views"));
			Path[] path = null;
			RotationWaypoint[] rw = null;
			NodeList pathList = elmt.getElementsByTagName("path");
			if (pathList.getLength() > 0) {
				path = new Path[pathList.getLength()];
				for (int i = 0; i < pathList.getLength(); i++) {
					Element pathElement = (Element) pathList.item(i);
					float pathx = Float.parseFloat(pathElement.getAttribute("x")) * TILE_SIZE * SCALE + TILE_SIZE / 2 * SCALE;
					float pathy = Float.parseFloat(pathElement.getAttribute("y")) * TILE_SIZE * SCALE + TILE_SIZE / 2 * SCALE;
					float speed = Float.parseFloat(pathElement.getAttribute("speed"));
					float delay = Float.parseFloat(pathElement.getAttribute("delay"));
					path[i] = new Path(pathx, pathy, speed, delay, null);
				}
			}
			
			NodeList rotationList = elmt.getElementsByTagName("rotation");
			if (rotationList.getLength() > 0) {
				rw = new RotationWaypoint[rotationList.getLength()];
				for (int i = 0; i < rotationList.getLength(); i++) {
					Element rotationElement = (Element) rotationList.item(i);
					float distance = Float.parseFloat(rotationElement.getAttribute("distance")) * pi / 180;
					String rotationDirection = rotationElement.getAttribute("direction");
					float turnrate = Float.parseFloat(rotationElement.getAttribute("turnrate"));
					float delay = Float.parseFloat(rotationElement.getAttribute("delay"));
					int rd = rotationDirection.equals("clockwise") ? CLOCKWISE : COUNTER_CLOCKWISE;
					rw[i] = new RotationWaypoint(distance, rd, turnrate, delay);
				}
			}
			
			//guards.add(new MultipleViewGuard(x, y, width, height, direction, path, rw, 0, views));
		} else if (type.equals("rotating")) {
			RotationWaypoint[] rw = null;
			NodeList rotationList = elmt.getElementsByTagName("rotation");
			if (rotationList.getLength() > 0) {
				rw = new RotationWaypoint[rotationList.getLength()];
				for (int i = 0; i < rotationList.getLength(); i++) {
					Element rotationElement = (Element) rotationList.item(i);
					float distance = Float.parseFloat(rotationElement.getAttribute("distance")) * pi / 180;
					String rotationDirection = rotationElement.getAttribute("direction");
					float turnrate = Float.parseFloat(rotationElement.getAttribute("turnrate"));
					float delay = Float.parseFloat(rotationElement.getAttribute("delay"));
					int rd = rotationDirection.equals("clockwise") ? CLOCKWISE : COUNTER_CLOCKWISE;
					rw[i] = new RotationWaypoint(distance, rd, turnrate, delay);
				}
			}
			
			guards.add(new RotatingGuard(x, y, width, height, direction, rw));
		} else if (type.equals("path")) {
			Path[] path = null;
			NodeList pathList = elmt.getElementsByTagName("path");
			if (pathList.getLength() > 0) {
				path = new Path[pathList.getLength()];
				for (int i = 0; i < pathList.getLength(); i++) {
					Element pathElement = (Element) pathList.item(i);
					float pathx = Float.parseFloat(pathElement.getAttribute("x")) * TILE_SIZE * SCALE + TILE_SIZE / 2 * SCALE;
					float pathy = Float.parseFloat(pathElement.getAttribute("y")) * TILE_SIZE * SCALE + TILE_SIZE / 2 * SCALE;
					float speed = Float.parseFloat(pathElement.getAttribute("speed"));
					float delay = Float.parseFloat(pathElement.getAttribute("delay"));
					RotationWaypoint[] rw = null;
					NodeList rotationList = pathElement.getElementsByTagName("rotation");
					if (rotationList.getLength() > 0) {
						rw = new RotationWaypoint[rotationList.getLength()];
						for (int j = 0; j < rotationList.getLength(); j++) {
							Element rotationElement = (Element) rotationList.item(j);
							float distance = Float.parseFloat(rotationElement.getAttribute("distance")) * pi / 180;
							String rotationDirection = rotationElement.getAttribute("direction");
							float turnrate = Float.parseFloat(rotationElement.getAttribute("turnrate"));
							float rotationDelay = Float.parseFloat(rotationElement.getAttribute("delay"));
							int rd = rotationDirection.equals("clockwise") ? CLOCKWISE : COUNTER_CLOCKWISE;
							rw[j] = new RotationWaypoint(distance, rd, turnrate, rotationDelay);
						}
					}
					
					path[i] = new Path(pathx, pathy, speed, delay, rw);
				}
			}
			
			guards.add(new PathGuard(x, y, width, height, direction, path, 0));
		}
	}
}
