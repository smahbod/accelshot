package com.koda.accelshot.handlers;

public class LoseCondition {

	public static final int REASON_TOO_SLOW = 0;
	public static final int REASON_SPOTTED = 1;
	public String message;
	public int reason;
	
	public LoseCondition(String message, int reason) {
		this.message = message;
		this.reason = reason;
	}
}
