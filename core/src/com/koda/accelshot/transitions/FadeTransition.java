package com.koda.accelshot.transitions;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.math.MathUtils;
import com.koda.accelshot.AccelShot;
import com.koda.accelshot.states.State;

public class FadeTransition extends Transition {

	private float timer;
	private float maxTime = .75f;
	private float alpha;
	
	@Override
	public void update(float dt) {
		timer += dt;
		
		if (phase == 0 & timer >= maxTime / 2)  {
			phase = 1;
			switched = true;
		}
		
		if (phase == 1 && timer >= maxTime) {
			phase = 2;
		}
	}

	@Override
	public void render(SpriteBatch sb) {
		if (phase == 0) {
			alpha = timer / (maxTime / 2);
		} else {
			alpha = MathUtils.clamp(1 - timer / (maxTime / 2), -1f, -.01f);
		}
		//sb.setProjectionMatrix(State.camera.combined);
		sb.begin();
		sb.setColor(1, 1, 1, alpha);
		sb.draw(AccelShot.res.getAtlas("pack").findRegion("black"), 0, 0, AccelShot.WIDTH, AccelShot.HEIGHT);
		sb.end();
		//sb.setColor(1, 1, 1, 1);
	}
}
