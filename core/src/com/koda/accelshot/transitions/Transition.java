package com.koda.accelshot.transitions;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.koda.accelshot.states.State;

public abstract class Transition {

	protected int phase;
	protected boolean switched;
	
	public boolean phaseSwitched() {
		if (switched) {
			switched = false;
			return true;
		}
		return false;
	}
	
	public int getPhase() {
		return phase;
	}

	public abstract void update(float dt);
	public abstract void render(SpriteBatch sb);
}
