package com.koda.accelshot;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.koda.accelshot.handlers.Content;
import com.koda.accelshot.handlers.EntityText;
import com.koda.accelshot.handlers.Save;
import com.koda.accelshot.states.GSM;
import com.koda.accelshot.states.MenuState;

public class AccelShot extends ApplicationAdapter {
	
	public static final String TITLE = "AccelShot";
	public static final int TILE_SIZE = 16;
	public static final float TILES_FOR_WIDTH = 25;
	public static final float TILES_FOR_HEIGHT = 13.5f;
	public static float WIDTH = 960;
	public static float HEIGHT = 540;
	public static float SCALE = WIDTH / TILE_SIZE / TILES_FOR_WIDTH;
	public static boolean DEBUG = false;
	private final boolean DRAW_FPS = false;
	private final boolean MEMORY = false;
	
	private SpriteBatch sb;
	
	private GSM gsm;
	
	public static Content res;

	public static EntityText etext;
	public static InputProcessor defaultInputProcessor;
	
	private int fps;
	private float memTimer = 5f;
	private float timer;
	private float maxTime = 0.05f;
	
	@Override
	public void create () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		
		sb = new SpriteBatch();
		etext = new EntityText();
		defaultInputProcessor = Gdx.input.getInputProcessor();
		
		res = new Content();
		res.loadAtlas("pack", "images.pack");
		
		Save.load();
		
		gsm = new GSM();
		//TODO: restore this line: 
		gsm.push(new MenuState(gsm));
		//gsm.push(new PlayState(gsm, 1));
	}
	
	@Override
	public void resize(int width, int height) {
		//System.out.println("Resized. Old WIDTH = " + AccelShot.WIDTH + " and HEIGHT is " + AccelShot.HEIGHT);
		float oldScale = SCALE;
		SCALE = WIDTH / TILE_SIZE / TILES_FOR_WIDTH;
		WIDTH = TILES_FOR_WIDTH * TILE_SIZE * SCALE;
		HEIGHT = TILES_FOR_HEIGHT * TILE_SIZE * SCALE;
		//System.out.println("New WIDTH = " + AccelShot.WIDTH + " and HEIGHT is " + AccelShot.HEIGHT);
		gsm.peek().updateScale(oldScale);
	}

	@Override
	public void render () {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		gsm.handleInput();
		gsm.update(Gdx.graphics.getDeltaTime());
		gsm.render(sb);
		
		if (DRAW_FPS) {
			calculateFPS(Gdx.graphics.getDeltaTime());
			sb.begin();
			etext.getFpsFont().draw(sb, "FPS: " + fps, 3, 20);
			sb.end();
		}
		
		if (MEMORY && memTimer > 0) {
			memTimer -= Gdx.graphics.getDeltaTime();
			if (memTimer <= 0) {
				memTimer = 5f;
				System.out.println("Using: " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1000000 + " MB");
			}
		}
	}
	
	private void calculateFPS(float dt) {
		timer += dt;
		if (timer >= maxTime) {
			timer = 0;
			fps = MathUtils.round(1f / dt);
		}
	}
}
